package de.unigoettingen.tradinghero.friends

import de.unigoettingen.trading_hero_core.jooq.tables.pojos.Person
import de.unigoettingen.tradinghero.TradingHeroRestAssured
import de.unigoettingen.tradinghero.persons.PersonRepository
import de.unigoettingen.tradinghero.persons.PersonService
import de.unigoettingen.tradinghero.utils.TestDataProvider
import io.quarkus.test.junit.QuarkusTest
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import java.time.OffsetDateTime.now
import javax.inject.Inject
import javax.ws.rs.core.Response

@QuarkusTest
internal class FriendshipResourceTest {

    @Inject
    lateinit var friendshipService: FriendshipService

    @Inject
    lateinit var personService: PersonService

    @Inject
    lateinit var personRepository: PersonRepository

    @BeforeEach
    fun setUp() {
        personService.createPerson(TestDataProvider.person)
        personService.createPerson(
            Person(
                "2",
                "Freund",
                "test@host.de",
                "testbild.com",
                "Tester",
                "Chuck",
                now(),
            )
        )
    }

    @AfterEach
    fun tearDown() {
        personRepository.removePerson("1")
        personRepository.removePerson("2")
    }

    @Test
    fun addRequest() {
        val beforeFriends = TradingHeroRestAssured
            .given()
            .`when`()
            .get("friendship")
            .then()
            .statusCode(Response.Status.OK.statusCode)
            .extract().jsonPath().getList<FriendRequestView>("")
        assertEquals(0, beforeFriends.size)
        val beforeRequests = TradingHeroRestAssured
            .given()
            .`when`()
            .get("friendship/requests")
            .then()
            .statusCode(Response.Status.OK.statusCode)
            .extract().jsonPath().getList<FriendRequestView>("")
        assertEquals(0, beforeRequests.size)

        val now = now()
        val request = TradingHeroRestAssured
            .given()
            .`when`()
            .post("friendship/2")
            .then()
            .statusCode(Response.Status.OK.statusCode)
            .extract()
            .`as`(FriendRequestView::class.java)

        assertEquals("2", request.friendId)
        assertTrue(request.dateCreated > now.toInstant().toEpochMilli())
        assertTrue(request.dateCreated < now().toInstant().toEpochMilli())

        val afterFriends = TradingHeroRestAssured
            .given()
            .`when`()
            .get("friendship")
            .then()
            .statusCode(Response.Status.OK.statusCode)
            .extract().jsonPath().getList<FriendRequestView>("")
        assertEquals(0, afterFriends.size)
        val afterRequests = TradingHeroRestAssured
            .given()
            .`when`()
            .get("friendship/requests")
            .then()
            .statusCode(Response.Status.OK.statusCode)
            .extract().jsonPath().getList<FriendRequestView>("")
        assertEquals(1, afterRequests.size)
    }

    @Test
    fun acceptFriend() {
        // 1 adds 2
        TradingHeroRestAssured
            .given()
            .`when`()
            .post("friendship/2")
            .then()
            .statusCode(Response.Status.OK.statusCode)
            .extract()
            .`as`(FriendRequestView::class.java)

        // 2 accepts 1 (No http call as we can not switch users inside tests for now)
        friendshipService.addFriend("2", "1")

        val afterFriends = TradingHeroRestAssured
            .given()
            .`when`()
            .get("friendship")
            .then()
            .statusCode(Response.Status.OK.statusCode)
            .extract().jsonPath().getList<FriendRequestView>("")
        assertEquals(1, afterFriends.size)
        val afterRequests = TradingHeroRestAssured
            .given()
            .`when`()
            .get("friendship/requests")
            .then()
            .statusCode(Response.Status.OK.statusCode)
            .extract().jsonPath().getList<FriendRequestView>("")
        assertEquals(0, afterRequests.size)
    }
}
