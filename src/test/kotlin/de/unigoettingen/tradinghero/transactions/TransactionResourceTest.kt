package de.unigoettingen.tradinghero.transactions

import de.unigoettingen.trading_hero_core.jooq.tables.Course.COURSE
import de.unigoettingen.trading_hero_core.jooq.tables.Stock.STOCK
import de.unigoettingen.trading_hero_core.jooq.tables.pojos.DepotEntry
import de.unigoettingen.tradinghero.TradingHeroRestAssured
import de.unigoettingen.tradinghero.balance.BalanceService
import de.unigoettingen.tradinghero.course.CourseRepository
import de.unigoettingen.tradinghero.course.CourseWriteView
import de.unigoettingen.tradinghero.depot.DepotRepository
import de.unigoettingen.tradinghero.persons.PersonRepository
import de.unigoettingen.tradinghero.persons.PersonService
import de.unigoettingen.tradinghero.stocks.StockRepository
import de.unigoettingen.tradinghero.utils.Configuration
import de.unigoettingen.tradinghero.utils.TestDataProvider
import io.quarkus.test.junit.QuarkusTest
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import java.time.OffsetDateTime
import javax.inject.Inject
import javax.ws.rs.core.Response

@QuarkusTest
internal class TransactionResourceTest {

    @Inject
    lateinit var stockRepository: StockRepository

    @Inject
    lateinit var personService: PersonService

    @Inject
    lateinit var personRepository: PersonRepository

    @Inject
    lateinit var courseRepository: CourseRepository

    @Inject
    lateinit var depotRepository: DepotRepository

    @Inject
    lateinit var balanceService: BalanceService

    @Inject
    lateinit var transactionRepository: TransactionRepository

    @Inject
    lateinit var configuration: Configuration

    @BeforeEach
    internal fun setUp() {
        personService.createPerson(TestDataProvider.person)
        stockRepository.updateStockMetaData(listOf(TestDataProvider.stock), true)
        courseRepository.updateStock(
            listOf(
                CourseWriteView(
                    TestDataProvider.stock.symbol,
                    2.0,
                    System.currentTimeMillis()
                )
            )
        )
    }

    @AfterEach
    internal fun tearDown() {
        personRepository.removePerson(TestDataProvider.person.id)
        courseRepository.jooq.dsl { db ->
            db.deleteFrom(COURSE)
                .where(COURSE.SYMBOL.eq(TestDataProvider.stock.symbol))
                .execute()
        }
        stockRepository.jooq.dsl { db ->
            db.deleteFrom(STOCK)
                .where(STOCK.SYMBOL.eq(TestDataProvider.stock.symbol))
                .execute()
        }
    }

    @Test
    fun buyAStock() {
        val symbol = TestDataProvider.stock.symbol
        val amount = 2.0

        val initialEntry = depotRepository.getDepotEntry(TestDataProvider.person.id, TestDataProvider.stock.symbol)
        assertNull(initialEntry)

        val transactionView =
            TradingHeroRestAssured
                .given()
                .body(TransactionWriteView(symbol, amount, TransactionType.BUY))
                .`when`()
                .post("transaction")
                .then()
                .statusCode(Response.Status.OK.statusCode)
                .extract()
                .`as`(TransactionView::class.java)

        assertEquals(symbol, transactionView.symbol)
        assertEquals(amount, transactionView.amount)
        val entry = depotRepository.getDepotEntry(TestDataProvider.person.id, TestDataProvider.stock.symbol)
        assertNotNull(entry)
        assertEquals(amount, entry?.amount)
        val balance = balanceService.getBalance(TestDataProvider.person.id)?.amount
        assertEquals(balance, configuration.startBalance.get() - amount * 2)
    }

    @Test
    fun buyAStockWithoutHavingEnoughBalance() {
        val symbol = TestDataProvider.stock.symbol
        val amount = 10000.0

        val initialEntry = depotRepository.getDepotEntry(TestDataProvider.person.id, TestDataProvider.stock.symbol)
        assertNull(initialEntry)

        TradingHeroRestAssured
            .given()
            .body(TransactionWriteView(symbol, amount, TransactionType.BUY))
            .`when`()
            .post("transaction")
            .then()
            .statusCode(422)
    }

    @Test
    fun sellAStock() {
        val symbol = TestDataProvider.stock.symbol
        val amount = 2.0

        depotRepository.addEntry(DepotEntry(symbol, TestDataProvider.person.id, amount))

        val transactionView =
            TradingHeroRestAssured
                .given()
                .body(TransactionWriteView(symbol, amount, TransactionType.SELL))
                .`when`()
                .post("transaction")
                .then()
                .statusCode(Response.Status.OK.statusCode)
                .extract()
                .`as`(TransactionView::class.java)

        assertEquals(symbol, transactionView.symbol)
        assertEquals(amount, transactionView.amount)
        val entry = depotRepository.getDepotEntry(TestDataProvider.person.id, TestDataProvider.stock.symbol)
        assertNotNull(entry)
        assertEquals(0.0, entry?.amount)
        val balance = balanceService.getBalance(TestDataProvider.person.id)?.amount
        assertEquals(balance, configuration.startBalance.get() + amount * 2)
    }

    @Test
    fun sellAStockWithoutOwningIt() {
        val symbol = TestDataProvider.stock.symbol
        val amount = 2.0
        TradingHeroRestAssured
            .given()
            .body(TransactionWriteView(symbol, amount, TransactionType.SELL))
            .`when`()
            .post("transaction")
            .then()
            .statusCode(422)
        val balance = balanceService.getBalance(TestDataProvider.person.id)?.amount
        assertEquals(balance, configuration.startBalance.get())
    }

    @Test
    fun sellAStockWithoutOwningEnough() {
        val symbol = TestDataProvider.stock.symbol
        val amount = 2.0
        depotRepository.addEntry(DepotEntry(symbol, TestDataProvider.person.id, amount / 2))
        TradingHeroRestAssured
            .given()
            .body(TransactionWriteView(symbol, amount, TransactionType.SELL))
            .`when`()
            .post("transaction")
            .then()
            .statusCode(422)
        val balance = balanceService.getBalance(TestDataProvider.person.id)?.amount
        assertEquals(balance, configuration.startBalance.get())
    }

    @Test
    fun getTransaction() {
        val symbol = TestDataProvider.stock.symbol
        val amount = 3.0
        val price = 2.0
        val transaction = transactionRepository.addTransaction(
            TestDataProvider.person.id,
            price,
            OffsetDateTime.now(),
            TransactionWriteView(symbol, amount, TransactionType.BUY)
        )
        assertNotNull(transaction)
        if (transaction != null) {
            val transactionView =
                TradingHeroRestAssured
                    .given()
                    .`when`()
                    .get("transaction/${transaction.id}")
                    .then()
                    .statusCode(Response.Status.OK.statusCode)
                    .extract()
                    .`as`(TransactionView::class.java)

            assertEquals(transaction.amount, transactionView.amount)
            assertEquals(transaction.price, transactionView.price)
            assertEquals(transaction.symbol, transactionView.symbol)
            assertEquals(TransactionType.fromDB(transaction.type), transactionView.type)
        }
    }

    @Test
    fun getTransactionHistory() {
        val symbol = TestDataProvider.stock.symbol
        val amount = 3.0
        val price = 2.0
        transactionRepository.addTransaction(
            TestDataProvider.person.id,
            price,
            OffsetDateTime.now(),
            TransactionWriteView(symbol, amount, TransactionType.BUY)
        )
        transactionRepository.addTransaction(
            TestDataProvider.person.id,
            price,
            OffsetDateTime.now(),
            TransactionWriteView(symbol, amount, TransactionType.BUY)
        )
        transactionRepository.addTransaction(
            TestDataProvider.person.id,
            price,
            OffsetDateTime.now(),
            TransactionWriteView(symbol, amount, TransactionType.SELL)
        )
        val transactionView =
            TradingHeroRestAssured
                .given()
                .`when`()
                .get("transaction/history/$symbol")
                .then()
                .statusCode(Response.Status.OK.statusCode)
                .extract()
                .`as`(TransactionsListView::class.java)

        assertEquals(3, transactionView.transactions.size)
        assertEquals(symbol, transactionView.symbol)
    }
}
