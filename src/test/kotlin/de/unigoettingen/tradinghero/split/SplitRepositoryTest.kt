package de.unigoettingen.tradinghero.split

import de.unigoettingen.trading_hero_core.jooq.tables.Course
import de.unigoettingen.trading_hero_core.jooq.tables.Stock
import de.unigoettingen.trading_hero_core.jooq.tables.pojos.Split
import de.unigoettingen.tradinghero.course.CourseRepository
import de.unigoettingen.tradinghero.course.CourseWriteView
import de.unigoettingen.tradinghero.depot.DepotService
import de.unigoettingen.tradinghero.persons.PersonRepository
import de.unigoettingen.tradinghero.persons.PersonService
import de.unigoettingen.tradinghero.stocks.StockRepository
import de.unigoettingen.tradinghero.transactions.TransactionService
import de.unigoettingen.tradinghero.utils.TestDataProvider
import io.quarkus.test.junit.QuarkusTest
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import javax.inject.Inject

@QuarkusTest
internal class SplitRepositoryTest {

    @Inject
    lateinit var splitRepository: SplitRepository

    @Inject
    lateinit var splitService: SplitService

    @Inject
    lateinit var depotService: DepotService

    @Inject
    lateinit var transactionService: TransactionService

    @Inject
    lateinit var personService: PersonService

    @Inject
    lateinit var stockRepository: StockRepository

    @Inject
    lateinit var courseRepository: CourseRepository

    @Inject
    lateinit var personRepository: PersonRepository

    @BeforeEach
    internal fun setUp() {
        personService.createPerson(TestDataProvider.person)
        stockRepository.updateStockMetaData(listOf(TestDataProvider.stock), true)
        courseRepository.updateStock(
            listOf(
                CourseWriteView(
                    TestDataProvider.stock.symbol,
                    2.0,
                    System.currentTimeMillis()
                )
            ),
            true
        )

        transactionService.makeTransaction(
            TestDataProvider.person.id,
            TestDataProvider.transactionWriteView
        )
    }

    @Test
    internal fun testSplit() {
        val testSplit = TestDataProvider.split

        splitRepository.insertSplit(testSplit)
        val repoSplit = splitRepository.getSplit(
            testSplit.symbol,
            testSplit.factor,
            testSplit.date
        )

        assertSplit(repoSplit, testSplit)

        splitService.updateStockAmountOfUsers()

        val entry = depotService.getDepotEntry(
            TestDataProvider.person.id,
            TestDataProvider.transactionWriteView.symbol
        )
        val newRepoSplit = splitRepository.getSplit(
            testSplit.symbol,
            testSplit.factor,
            testSplit.date
        )

        assertEquals(newRepoSplit?.executed, true)
        assertEquals(newRepoSplit?.symbol, repoSplit?.symbol)
        assertEquals(newRepoSplit?.factor, repoSplit?.factor)
        assertEquals(newRepoSplit?.date, repoSplit?.date)

        assertEquals(entry?.amount, TestDataProvider.transactionWriteView.amount * testSplit.factor)
    }

    @AfterEach
    internal fun down() {
        personRepository.removePerson(TestDataProvider.person.id)
        courseRepository.jooq.dsl { db ->
            db.deleteFrom(Course.COURSE)
                .where(Course.COURSE.SYMBOL.eq(TestDataProvider.stock.symbol))
                .execute()
        }
        stockRepository.jooq.dsl { db ->
            db.deleteFrom(Stock.STOCK)
                .where(Stock.STOCK.SYMBOL.eq(TestDataProvider.stock.symbol))
                .execute()
        }
    }

    fun assertSplit(split1: Split?, split2: Split?) {
        assertEquals(split1?.symbol, split2?.symbol)
        assertEquals(split1?.date?.toEpochSecond(), split2?.date?.toEpochSecond())
        assertEquals(split1?.factor, split2?.factor)
        assertEquals(split1?.executed, split2?.executed)
    }
}
