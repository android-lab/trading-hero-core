package de.unigoettingen.tradinghero

import com.fasterxml.jackson.databind.ObjectMapper
import de.unigoettingen.tradinghero.resteasyjackson.MyObjectMapperCustomizer
import io.restassured.RestAssured
import io.restassured.config.ObjectMapperConfig
import io.restassured.config.RestAssuredConfig
import io.restassured.http.ContentType
import io.restassured.parsing.Parser
import io.restassured.specification.RequestSpecification
import java.lang.reflect.Type
import javax.ws.rs.core.MediaType

class TradingHeroRestAssured : RestAssured() {

    companion object {
        private val objectMapperConfig: ObjectMapperConfig =
            config.objectMapperConfig.jackson2ObjectMapperFactory { _: Type, _: String ->
                ObjectMapper().also { MyObjectMapperCustomizer().customize(it) }
            }

        fun given(): RequestSpecification {
            defaultParser = Parser.JSON
            return RestAssured.given()
                .basePath("")
                .header("Content-Type", ContentType.JSON)
                .accept(MediaType.APPLICATION_JSON)
                .config(RestAssuredConfig.config().objectMapperConfig(objectMapperConfig))
        }
    }
}
