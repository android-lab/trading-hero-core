package de.unigoettingen.tradinghero.depot

import de.unigoettingen.trading_hero_core.jooq.tables.DepotEntry.DEPOT_ENTRY
import de.unigoettingen.trading_hero_core.jooq.tables.Stock.STOCK
import de.unigoettingen.trading_hero_core.jooq.tables.pojos.DepotEntry
import de.unigoettingen.tradinghero.database.Jooq
import de.unigoettingen.tradinghero.persons.PersonRepository
import de.unigoettingen.tradinghero.utils.DBHelper
import de.unigoettingen.tradinghero.utils.TestDataProvider
import io.quarkus.test.junit.QuarkusTest
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import javax.inject.Inject

@QuarkusTest
internal class DepotRepositoryTest {

    @Inject
    lateinit var depotRepository: DepotRepository

    @Inject
    lateinit var jooq: Jooq

    @Inject
    lateinit var personRepository: PersonRepository

    @Test
    fun addAndGetDepotEntry() {
        try {
            // Setup test data
            jooq.dsl { db ->
                db.insertInto(STOCK).set(DBHelper.listProperties(TestDataProvider.stock))
                    .execute()
            }
            personRepository.createPerson(TestDataProvider.person)
            val entry = DepotEntry(TestDataProvider.stock.symbol, TestDataProvider.person.id, 10.0)
            depotRepository.addEntry(entry)

            // Test
            val dbEntries = depotRepository.getDepot(TestDataProvider.person.id)

            assertEquals(1, dbEntries.size)
            assertEquals(entry.amount, dbEntries[0].amount)
            assertEquals(entry.personId, dbEntries[0].personId)
            assertEquals(entry.symbol, dbEntries[0].symbol)
        } finally {
            // Cleanup
            jooq.dsl { db ->
                db.deleteFrom(STOCK).where(STOCK.SYMBOL.eq(TestDataProvider.stock.symbol)).execute()
            }
            personRepository.removePerson(TestDataProvider.person.id)
            jooq.dsl { db ->
                db.deleteFrom(DEPOT_ENTRY).where(DEPOT_ENTRY.PERSON_ID.eq(TestDataProvider.person.id)).execute()
            }
        }
    }

    @Test
    fun addAndGetSingleDepotEntry() {
        try {
            // Setup test data
            jooq.dsl { db ->
                db.insertInto(STOCK).set(DBHelper.listProperties(TestDataProvider.stock))
                    .execute()
            }
            personRepository.createPerson(TestDataProvider.person)
            val entry = DepotEntry(TestDataProvider.stock.symbol, TestDataProvider.person.id, 10.0)
            depotRepository.addEntry(entry)

            // Test
            val dbEntry = depotRepository.getDepotEntry(TestDataProvider.person.id, TestDataProvider.stock.symbol)

            assertEquals(entry.amount, dbEntry?.amount)
            assertEquals(entry.personId, dbEntry?.personId)
            assertEquals(entry.symbol, dbEntry?.symbol)
        } finally {
            // Cleanup
            jooq.dsl { db ->
                db.deleteFrom(STOCK).where(STOCK.SYMBOL.eq(TestDataProvider.stock.symbol)).execute()
            }
            personRepository.removePerson(TestDataProvider.person.id)
            jooq.dsl { db ->
                db.deleteFrom(DEPOT_ENTRY).where(DEPOT_ENTRY.PERSON_ID.eq(TestDataProvider.person.id)).execute()
            }
        }
    }
}
