package de.unigoettingen.tradinghero.persons

import de.unigoettingen.trading_hero_core.jooq.tables.pojos.Person
import io.quarkus.test.junit.QuarkusTest
import org.jooq.exception.DataAccessException
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test
import java.time.OffsetDateTime.now
import java.time.temporal.ChronoUnit
import java.util.*
import javax.inject.Inject

@QuarkusTest
internal class PersonRepositoryTest {

    @Inject
    lateinit var personRepository: PersonRepository

    @Test
    fun addPerson() {
        val id = UUID.randomUUID().toString()
        val person = Person(id, "", "", "", "", "", now())
        try {
            val personFromSaveOperation = personRepository.createPerson(person)
            val personFromDB = personRepository.getPerson(id)
            // Should return saved data
            personEquals(person, personFromSaveOperation)
            // No duplicates
            assertThrows(DataAccessException::class.java) { personRepository.createPerson(person) }
            // Should save data
            personEquals(person, personFromDB)
        } finally {
            personRepository.removePerson(id)
        }
    }

    @Test
    fun existPerson() {
        val id = UUID.randomUUID().toString()
        val person = Person(id, "", "", "", "", "", now())
        assertFalse(personRepository.existPerson(id))
        try {
            personRepository.createPerson(person)
            assert(personRepository.existPerson(id))
        } finally {
            personRepository.removePerson(id)
        }
    }

    @Test
    fun getPerson() {
        val id = UUID.randomUUID().toString()
        val person = Person(id, "", "", "", "", "", now())
        try {
            assertNull(personRepository.getPerson(id))
            personRepository.createPerson(person)
            val personFromDB = personRepository.getPerson(id)
            personEquals(person, personFromDB)
        } finally {
            personRepository.removePerson(id)
        }
    }

    @Test
    fun updatePerson() {
        val id = UUID.randomUUID().toString()
        val person = Person(id, "Testname", "", "", "", "", now())
        try {
            personRepository.createPerson(person)
            val originalPerson = personRepository.getPerson(id)
            val operationPerson = personRepository.updatePerson(id, PersonWriteView("AndererName", "", "", ""))
            val dbPerson = personRepository.getPerson(id)
            assertEquals("AndererName", operationPerson?.name)
            assertEquals("AndererName", dbPerson?.name)
            assertEquals("Testname", originalPerson?.name)
        } finally {
            personRepository.removePerson(id)
        }
    }

    fun personEquals(person1: Person?, person2: Person?) {
        assertNotNull(person1)
        assertNotNull(person2)
        if (person1 != null && person2 != null) {
            assertEquals(person1.id, person2.id)
            assertEquals(person1.name, person2.name)
            assertEquals(person1.givenName, person2.givenName)
            assertEquals(person1.familyName, person2.familyName)
            assertEquals(person1.email, person2.email)
            assertEquals(person1.dateCreated.truncatedTo(ChronoUnit.SECONDS), person2.dateCreated.truncatedTo(ChronoUnit.SECONDS))
            assertEquals(person1.image, person2.image)
        }
    }
}
