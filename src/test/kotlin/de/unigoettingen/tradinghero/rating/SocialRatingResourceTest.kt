package de.unigoettingen.tradinghero.rating

import de.unigoettingen.trading_hero_core.jooq.tables.Stock.STOCK
import de.unigoettingen.trading_hero_core.jooq.tables.pojos.Rating
import de.unigoettingen.tradinghero.TradingHeroRestAssured
import de.unigoettingen.tradinghero.stocks.StockRepository
import de.unigoettingen.tradinghero.utils.TestDataProvider
import io.quarkus.test.junit.QuarkusTest
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import java.time.OffsetDateTime.now
import javax.inject.Inject

@QuarkusTest
class SocialRatingResourceTest {
    @Inject
    lateinit var stockRepository: StockRepository

    @Inject
    lateinit var marketplaceRepository: RatingRepository

    @BeforeEach
    fun setUp() {
        stockRepository.updateStockMetaData(listOf(TestDataProvider.stock), true)
    }

    @Test
    fun invalidCallTest() {
        TradingHeroRestAssured
            .given()
            .queryParam("amount", -1)
            .`when`()
            .get("rating/top/${RatingType.SOCIAL.name}")
            .then()
            .statusCode(422)

        TradingHeroRestAssured
            .given()
            .queryParam("amount", 0)
            .`when`()
            .get("rating/top/${RatingType.SOCIAL.name}")
            .then()
            .statusCode(422)
    }

    @Test
    fun validCallTest() {
        TradingHeroRestAssured
            .given()
            .queryParam("amount", 10)
            .`when`()
            .get("rating/top/${RatingType.SOCIAL.name}")
            .then()
            .statusCode(200)

        TradingHeroRestAssured
            .given()
            .`when`()
            .get("rating/top/${RatingType.SOCIAL.name}")
            .then()
            .statusCode(200)
    }

    @Test
    fun testPlacement() {
        // insert sample data with unreachable high score

        val testDataRatings = listOf(
            Rating(
                TestDataProvider.stock.symbol,
                now(),
                0.0,
                0.0,
                10.0
            )
        )

        marketplaceRepository.updateSocialRatings(testDataRatings)

        val ratings = TradingHeroRestAssured
            .given()
            .queryParam("amount", 1)
            .`when`()
            .get("rating/top/${RatingType.SOCIAL.name}")
            .then()
            .statusCode(200)
            .extract()
            .`as`(emptyArray<RatingView>()::class.java)

        assertEquals(ratings.size, 1)
        assertEquals(ratings?.firstOrNull()?.placement, 1)
        assertEquals(ratings?.firstOrNull()?.stock, TestDataProvider.stock.symbol)

        val response = TradingHeroRestAssured
            .given()
            .queryParam("amount", 1)
            .`when`()
            .get("rating/${RatingType.SOCIAL.name}/${TestDataProvider.stock.symbol}")
            .then()
            .statusCode(200)
            .extract()
            .`as`(RatingView::class.java)

        assertEquals(response.stock, TestDataProvider.stock.symbol)
        assertEquals(response.placement, 1)
    }

    @AfterEach
    fun tearDown() {
        stockRepository.jooq.dsl { db ->
            db.deleteFrom(STOCK).where(STOCK.SYMBOL.eq(TestDataProvider.stock.symbol))
                .execute()
        }
    }
}
