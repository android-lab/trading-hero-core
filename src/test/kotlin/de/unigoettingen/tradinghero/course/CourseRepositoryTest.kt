package de.unigoettingen.tradinghero.course

import de.unigoettingen.trading_hero_core.jooq.tables.Course.COURSE
import de.unigoettingen.trading_hero_core.jooq.tables.Stock.STOCK
import de.unigoettingen.trading_hero_core.jooq.tables.pojos.Course
import de.unigoettingen.tradinghero.stocks.StockRepository
import de.unigoettingen.tradinghero.utils.TestDataProvider
import io.quarkus.test.junit.QuarkusTest
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import javax.inject.Inject
import kotlin.math.pow

@QuarkusTest
internal class CourseRepositoryTest {
    @Inject
    lateinit var courseRepository: CourseRepository

    @Inject
    lateinit var stockRepository: StockRepository

    @BeforeEach
    fun setUp() {
        stockRepository.updateStockMetaData(listOf(TestDataProvider.stock), true)
    }

    @AfterEach
    fun tearDown() {
        stockRepository.jooq.dsl { db ->
            db.deleteFrom(STOCK)
                .where(STOCK.SYMBOL.eq(TestDataProvider.stock.symbol))
                .execute()
        }
    }

    @Test
    fun testInsert() {
        val course = TestDataProvider.course

        toCourseWriteView(course)?.let {
            courseRepository.updateStock(listOf(it), true)
        }

        val repoCourse = courseRepository.jooq.dsl { db ->
            db.selectFrom(COURSE)
                .where(COURSE.SYMBOL.eq(course.symbol))
                .fetchOneInto(Course::class.java)
        }

        assertEquals(toCourseWriteView(repoCourse), toCourseWriteView(course))

        courseRepository.jooq.dsl { db ->
            db.deleteFrom(COURSE)
                .where(COURSE.SYMBOL.eq(course.symbol))
                .execute()
        }
    }

    @Test
    fun testLastTimestampCourse() {
        val courses = (0..100).map {
            Course(TestDataProvider.stock.symbol, 2.0.pow(it).toLong() % 756, it.toDouble())
        }

        courseRepository.updateStock(
            courses.mapNotNull { toCourseWriteView(it) },
            true
        )

        assertEquals(
            courses.maxOfOrNull { it.timestamp },
            courseRepository.getLastStockUpdate(TestDataProvider.stock.symbol)
        )
        assertEquals(
            toCourseWriteView(courses.maxByOrNull { it.timestamp }),
            toCourseWriteView(courseRepository.getLastStockPrice(TestDataProvider.stock.symbol))
        )

        courseRepository.jooq.dsl { db ->
            db.deleteFrom(COURSE)
                .where(COURSE.SYMBOL.eq(TestDataProvider.stock.symbol))
                .execute()
        }
    }

    fun toCourseWriteView(course: Course?) = course?.let {
        with(it) {
            CourseWriteView(symbol, price, timestamp)
        }
    }
}
