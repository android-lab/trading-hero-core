package de.unigoettingen.tradinghero.stocks

data class TinyStockView(
    val symbol: String,
    val name: String,
    val logo: String,
    val lastStockPrice: Double?,
)
