package de.unigoettingen.tradinghero.stocks

import de.unigoettingen.trading_hero_core.jooq.tables.pojos.Course
import de.unigoettingen.trading_hero_core.jooq.tables.pojos.Stock
import de.unigoettingen.tradinghero.course.CourseService
import de.unigoettingen.tradinghero.course.CourseView
import javax.inject.Inject
import javax.ws.rs.*
import javax.ws.rs.core.MediaType

@Path("/stocks")
@Produces(MediaType.APPLICATION_JSON)
class StockResource {

    @Inject
    lateinit var stockService: StockService

    @Inject
    lateinit var courseService: CourseService

    @GET
    @Path("/list")
    fun getAllStockSymbols(): List<TinyStockView> =
        stockService.tinyList().map { this.stockService.toTinyStockView(it, null) }

    @GET
    @Path("/information/{symbol}")
    fun getStockInformation(@PathParam("symbol") symbol: String, @QueryParam("currency") currency: String?) =
        stockService.getStockInformation(symbol, currency)?.let { toStockView(it) }

    @GET
    @Path("/pricing/{symbol}")
    fun getCourse(
        @PathParam("symbol") symbol: String,
        @QueryParam("greaterDiffFromNow") greaterDiffFromNow: Long?, // in millis
        @QueryParam("lesserDiffFromNow") lesserDiffFromNow: Long?, // in millis
        @QueryParam("limit") limit: Int?,
        @QueryParam("currency") currency: String?
    ): List<CourseView> =
        courseService.getCourse(
            symbol,
            greaterDiffFromNow,
            lesserDiffFromNow,
            limit,
            currency
        ).map { toCourseView(it) }

    @GET
    @Path("/pricing/current/{symbol}")
    fun getCurrentStockPricing(
        @PathParam("symbol") symbol: String,
        @QueryParam("currency") currency: String?
    ): CourseView? = courseService.getLastStockPrice(symbol, currency)?.let { toCourseView(it) }

    @GET
    @Path("/search/{search}")
    fun getSearchedStocks(
        @PathParam("search") search: String,
        @QueryParam("price") price: Boolean?,
        @QueryParam("currency") currency: String?
    ): List<TinyStockView> =
        stockService.search(search, price != null && price, currency)

    companion object {
        private fun toStockView(stock: Stock): StockView {
            return with(stock) {
                StockView(
                    symbol,
                    currency,
                    name,
                    description,
                    figi,
                    mic,
                    displaySymbol,
                    type,
                    exchange,
                    industry,
                    ipo,
                    logo,
                    marketcapitalization,
                    shareoutstanding,
                    url
                )
            }
        }

        private fun toCourseView(course: Course): CourseView {
            return with(course) {
                CourseView(
                    symbol,
                    timestamp,
                    price
                )
            }
        }
    }
}
