package de.unigoettingen.tradinghero.stocks

import java.sql.Date

data class StockView(
    val symbol: String,
    val currency: String,
    val name: String,
    val description: String,
    val figi: String,
    val mic: String,
    val displaySymbol: String,
    val type: String,
    val exchange: String,
    val industry: String,
    val ipo: Date,
    val logo: String,
    val marketCapitalization: Double,
    val shareOutstanding: Double,
    val url: String,
)
