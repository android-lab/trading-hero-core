package de.unigoettingen.tradinghero.stocks

import io.quarkus.arc.config.ConfigProperties

@ConfigProperties(prefix = "stocks")
class StockPreset {
    lateinit var exchanges: MutableList<String>
    lateinit var names: MutableList<String>

    override fun toString(): String {
        return "Exchanges $exchanges  Names $names"
    }
}
