package de.unigoettingen.tradinghero.stocks

import de.unigoettingen.trading_hero_core.jooq.tables.Stock.STOCK
import de.unigoettingen.trading_hero_core.jooq.tables.pojos.Stock
import de.unigoettingen.tradinghero.database.Jooq
import de.unigoettingen.tradinghero.utils.DBHelper
import javax.enterprise.context.ApplicationScoped
import javax.inject.Inject

@ApplicationScoped
class StockRepository {

    @set:Inject
    lateinit var jooq: Jooq

    fun updateStockMetaData(stocks: List<Stock>, sync: Boolean = false) {
        jooq.dsl { db ->
            stocks.forEach { stock ->
                val query = db.insertInto(STOCK)
                    .set(DBHelper.listProperties(stock))
                    .onConflict(STOCK.SYMBOL)
                    .doUpdate()
                    .set(DBHelper.listProperties(stock))
                if (sync) {
                    query.execute()
                } else {
                    query.executeAsync()
                }
            }
        }
    }

    fun listAllStocks(): List<Stock> {
        return jooq.dsl { db ->
            db.fetch(STOCK).into(Stock::class.java)
        }
    }

    fun listAllTinyStocks(): List<Stock> {
        return jooq.dsl { db ->
            db.select(STOCK.NAME, STOCK.SYMBOL, STOCK.MARKETCAPITALIZATION, STOCK.CURRENCY, STOCK.LOGO)
                .from(STOCK)
                .fetchInto(Stock::class.java)
        }
    }

    fun getStockInformation(symbol: String): Stock? {
        return jooq.dsl { db ->
            db.selectFrom(STOCK)
                .where(STOCK.SYMBOL.eq(symbol))
                .fetchOneInto(Stock::class.java)
        }
    }
}
