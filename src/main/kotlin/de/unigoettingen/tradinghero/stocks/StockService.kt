package de.unigoettingen.tradinghero.stocks

import de.unigoettingen.trading_hero_core.jooq.tables.pojos.Stock
import de.unigoettingen.tradinghero.course.CourseService
import de.unigoettingen.tradinghero.currency.CurrencyService
import de.unigoettingen.tradinghero.datasource.APICallRequester
import de.unigoettingen.tradinghero.datasource.finnhub.FinnhubRestClient
import de.unigoettingen.tradinghero.datasource.finnhub.FinnhubService
import de.unigoettingen.tradinghero.datasource.finnhub.toStock
import de.unigoettingen.tradinghero.utils.Configuration
import io.quarkus.scheduler.Scheduled
import java.sql.Date
import java.text.SimpleDateFormat
import javax.enterprise.context.ApplicationScoped
import javax.inject.Inject

@ApplicationScoped
class StockService {
    @Inject
    lateinit var finnhubRestClient: FinnhubRestClient

    @Inject
    lateinit var stockRepository: StockRepository

    @Inject
    lateinit var stockPreset: StockPreset

    @Inject
    lateinit var finnhubService: FinnhubService

    @Inject
    lateinit var courseService: CourseService

    @Inject
    lateinit var currencyService: CurrencyService

    @Inject
    lateinit var configuration: Configuration

    @Scheduled(every = "P7D")
    fun updateStockDatabase() {
        val stocks = mutableListOf<Stock>()

        finnhubService.block(APICallRequester.STOCK)
        // fetch general stock data
        stockPreset.exchanges.forEach { exchange ->
            val usFinnhubStocks = finnhubService.scheduleAPICall(requester = APICallRequester.STOCK) {
                finnhubRestClient
                    .getAllStocks(exchange)
            }

            if (usFinnhubStocks.isEmpty()) {
                println("Error: Failed to find exchange \"$exchange\"")
                finnhubService.free()
                return@forEach
            }

            stocks.addAll(
                usFinnhubStocks
                    .filter { stockPreset.names.contains(it.symbol) }
                    .map { toStock(it) }
            )
        }

        // fetch advanced stock data

        val ipoDate = SimpleDateFormat("yyyy-MM-dd")

        stocks.forEach {
            val advancedStockData = finnhubService.scheduleAPICall(requester = APICallRequester.STOCK) {
                finnhubRestClient.getProfile(it.symbol)
            }

            with(advancedStockData) {
                it.currency = currency
                it.exchange = exchange
                it.industry = finnhubIndustry
                it.ipo = Date(ipoDate.parse(ipo).time)
                it.name = name
                it.logo = logo
                it.url = weburl
                it.marketcapitalization = currencyService.toCurrency(
                    currency,
                    configuration.baseCurrency.get(),
                    marketCapitalization
                )
                it.shareoutstanding = shareOutstanding
            }
        }

        finnhubService.free()
        stockRepository.updateStockMetaData(stocks)
        println("Updated ${stocks.size} Stock-Information")
    }

    fun search(search: String, priceIncluded: Boolean, currency: String?): List<TinyStockView> {
        val searchResult = tinyList().filter {
            it.name.toLowerCase().contains(search.toLowerCase()) ||
                it.symbol.toLowerCase().contains(search.toLowerCase())
        }
        val lastStockPrices =
            if (priceIncluded) courseService.getLastStockPrices(
                searchResult.map { it.symbol },
                currency
            ) else emptyList()

        return searchResult.sortedByDescending {
            currencyService.toCurrency(
                it.currency,
                "USD",
                it.marketcapitalization
            )
        }
            .map {
                toTinyStockView(
                    it,
                    lastStockPrices
                        .find { stockPrice -> stockPrice?.symbol == it.symbol }?.price
                )
            }
    }

    fun getStockInformation(symbol: String, currency: String?) =
        stockRepository.getStockInformation(symbol)?.apply {
            marketcapitalization =
                currencyService.toCurrency(
                    from = configuration.baseCurrency.get(),
                    to = currency ?: configuration.baseCurrency.get(),
                    amount = marketcapitalization
                )
        }

    fun list(): List<Stock> = stockRepository.listAllStocks()

    fun tinyList() = stockRepository.listAllTinyStocks()

    fun toTinyStockView(stock: Stock, lastStockPrice: Double?): TinyStockView {
        return with(stock) {
            TinyStockView(symbol, name, stock.logo, lastStockPrice)
        }
    }
}
