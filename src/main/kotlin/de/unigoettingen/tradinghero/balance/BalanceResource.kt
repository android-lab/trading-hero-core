package de.unigoettingen.tradinghero.balance

import de.unigoettingen.trading_hero_core.jooq.tables.pojos.Balance
import de.unigoettingen.tradinghero.auth.getUserId
import javax.inject.Inject
import javax.ws.rs.GET
import javax.ws.rs.Path
import javax.ws.rs.Produces
import javax.ws.rs.QueryParam
import javax.ws.rs.core.Context
import javax.ws.rs.core.MediaType
import javax.ws.rs.core.SecurityContext

@Path("/balance")
@Produces(MediaType.APPLICATION_JSON)
class BalanceResource {

    @Inject
    lateinit var balanceService: BalanceService

    @Context
    lateinit var ctx: SecurityContext

    @GET
    fun getBalance(@QueryParam("currency") currency: String?) =
        balanceService.getBalance(getUserId(ctx), currency)?.let { toView(it) }

    companion object {
        fun toView(balance: Balance): BalanceView {
            with(balance) {
                return BalanceView(personId, amount)
            }
        }
    }
}
