package de.unigoettingen.tradinghero.balance

data class BalanceView(val personID: String, val amount: Double)
