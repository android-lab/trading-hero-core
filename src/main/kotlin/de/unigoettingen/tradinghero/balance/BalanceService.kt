package de.unigoettingen.tradinghero.balance

import de.unigoettingen.trading_hero_core.jooq.tables.pojos.Balance
import de.unigoettingen.tradinghero.currency.CurrencyService
import de.unigoettingen.tradinghero.exceptions.BalanceException
import de.unigoettingen.tradinghero.exceptions.BalanceNotInitializedException
import de.unigoettingen.tradinghero.utils.Configuration
import javax.enterprise.context.ApplicationScoped
import javax.inject.Inject

@ApplicationScoped
class BalanceService {

    @Inject
    lateinit var balanceRepository: BalanceRepository

    @Inject
    lateinit var currencyService: CurrencyService

    @Inject
    lateinit var configuration: Configuration

    fun createStarterBalance(personID: String): Balance? = balanceRepository.createStarterBalance(personID)

    fun existBalance(personID: String): Boolean = balanceRepository.existBalance(personID)

    fun getBalance(personID: String, currency: String? = null): Balance? {
        return balanceRepository.getBalance(personID)
            ?.apply {
                amount = currencyService.toCurrency(
                    from = configuration.baseCurrency.get(),
                    to = currency ?: configuration.baseCurrency.get(),
                    amount = amount
                )
            }
    }

    fun addToBalance(personID: String, amount: Double): Balance? {
        val balance = getBalance(personID)
        balance?.let {
            if (it.amount + amount > 0) {
                return balanceRepository.updateBalance(Balance(personID, it.amount + amount))
            } else {
                throw BalanceException("Balance is too low!")
            }
        } ?: throw BalanceNotInitializedException()
    }
}
