package de.unigoettingen.tradinghero.balance

import de.unigoettingen.trading_hero_core.jooq.tables.Balance.BALANCE
import de.unigoettingen.trading_hero_core.jooq.tables.pojos.Balance
import de.unigoettingen.tradinghero.database.Jooq
import de.unigoettingen.tradinghero.utils.Configuration
import de.unigoettingen.tradinghero.utils.DBHelper
import javax.enterprise.context.ApplicationScoped
import javax.inject.Inject

@ApplicationScoped
class BalanceRepository {
    @set:Inject
    lateinit var jooq: Jooq

    @Inject
    lateinit var configuration: Configuration

    fun createStarterBalance(personID: String): Balance? =
        jooq.dsl { db ->
            db.insertInto(BALANCE)
                .set(DBHelper.listProperties(Balance(personID, configuration.startBalance.get())))
                .returning().fetchOneInto(Balance::class.java)
        }

    fun existBalance(personID: String): Boolean = jooq.dsl { db ->
        db.fetchExists(
            db.selectFrom(BALANCE).where(
                BALANCE.PERSON_ID.eq(personID)
            )
        )
    }

    fun getBalance(personID: String): Balance? = jooq.dsl { db ->
        db.selectFrom(BALANCE).where(BALANCE.PERSON_ID.eq(personID)).fetchOneInto(Balance::class.java)
    }

    fun updateBalance(balance: Balance): Balance? = jooq.dsl { db ->
        db.update(BALANCE).set(DBHelper.listProperties(balance))
            .where(BALANCE.PERSON_ID.eq(balance.personId))
            .returning()
            .fetchOneInto(Balance::class.java)
    }
}
