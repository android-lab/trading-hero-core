package de.unigoettingen.tradinghero.rating

import de.unigoettingen.trading_hero_core.jooq.tables.pojos.Rating
import de.unigoettingen.tradinghero.datasource.finnhub.FinnhubRatingView
import de.unigoettingen.tradinghero.datasource.finnhub.FinnhubRestClient
import de.unigoettingen.tradinghero.datasource.finnhub.FinnhubService
import de.unigoettingen.tradinghero.exceptions.InvalidAmountException
import de.unigoettingen.tradinghero.exceptions.UnknownRatingException
import de.unigoettingen.tradinghero.stocks.StockPreset
import io.quarkus.scheduler.Scheduled
import java.time.OffsetDateTime
import java.time.ZoneOffset
import java.time.format.DateTimeFormatterBuilder
import java.time.temporal.ChronoField
import javax.enterprise.context.ApplicationScoped
import javax.inject.Inject

@ApplicationScoped
class RatingService {
    @Inject
    lateinit var finnhubRestClient: FinnhubRestClient

    @Inject
    lateinit var finnhubService: FinnhubService

    @Inject
    lateinit var stockPreset: StockPreset

    @Inject
    lateinit var ratingRepository: RatingRepository

    private val offsetDateTimeFormatter = DateTimeFormatterBuilder()
        .appendPattern("yyyy-MM-dd HH:mm:ss")
        .parseDefaulting(ChronoField.HOUR_OF_DAY, 0)
        .parseDefaulting(ChronoField.MINUTE_OF_HOUR, 0)
        .parseDefaulting(ChronoField.SECOND_OF_MINUTE, 0)
        .parseDefaulting(ChronoField.MILLI_OF_SECOND, 0)
        .toFormatter()
        .withZone(ZoneOffset.UTC)

    @Scheduled(every = "1h")
    fun loadRatings() {
        stockPreset.names.forEach {
            val rating = finnhubService.scheduleAPICall {
                finnhubRestClient.getSocialSentiment(it)
            }

            ratingRepository.updateSocialRatings(toSocialRatingEntries(rating))
        }
    }

    fun getTop(ratingType: RatingType, amount: Int?): List<RatingView> {
        if (amount?.let { it <= 0 } == true) {
            throw InvalidAmountException()
        }

        return when (ratingType) {
            RatingType.SOCIAL -> getTopSocialRatings(amount ?: 20)
            RatingType.MARKET_CAP -> getTopMarketCap(amount ?: 20)
            RatingType.PRICE -> getTopPrice(amount ?: 20)
        }
    }

    fun getTopPrice(amount: Int): List<RatingView> = ratingRepository.getTopPrices(amount)
        .mapIndexed { index, course -> toRatingView(index + 1, course.symbol) }

    fun getTopMarketCap(amount: Int): List<RatingView> = ratingRepository.getTopMarketCap(amount)
        .mapIndexed { index, stock -> toRatingView(index + 1, stock.symbol) }

    fun getTopSocialRatings(amount: Int): List<RatingView> = ratingRepository.getTopSocial(amount)
        .mapIndexed { index, marketplace -> toRatingView(index + 1, marketplace) }

    fun getPlacement(type: RatingType, symbol: String): RatingView =
        when (type) {
            RatingType.SOCIAL -> getSocialPlacement(symbol)
            RatingType.MARKET_CAP -> getMarketCapPlacement(symbol)
            RatingType.PRICE -> getPricePlacement(symbol)
        }

    fun getSocialPlacement(symbol: String): RatingView =
        ratingRepository.getSocialPlacement(symbol)?.takeIf { it.toInt() > 0 }?.let { toRatingView(it.toInt(), symbol) }
            ?: throw UnknownRatingException()

    fun getPricePlacement(symbol: String): RatingView =
        ratingRepository.getPricePlacement(symbol)?.takeIf { it.toInt() > 0 }?.let { toRatingView(it.toInt(), symbol) }
            ?: throw UnknownRatingException()

    fun getMarketCapPlacement(symbol: String): RatingView =
        ratingRepository.getMarketCapPlacement(symbol)?.takeIf { it.toInt() > 0 }
            ?.let { toRatingView(it.toInt(), symbol) }
            ?: throw UnknownRatingException()

    fun toRatingView(placement: Int, stock: String) =
        RatingView(placement = placement, stock = stock)

    fun toRatingView(placement: Int, rating: Rating) = with(rating) {
        RatingView(placement = placement, stock = stock)
    }

    fun toSocialRatingEntries(finnhubRatingView: FinnhubRatingView) = with(finnhubRatingView) {
        reddit.map { redditEntry ->
            val redditDate = OffsetDateTime.parse(redditEntry.atTime, offsetDateTimeFormatter)
            val twitterEntry =
                twitter.minByOrNull {
                    kotlin.math.abs(
                        redditDate.toEpochSecond() -
                            OffsetDateTime.parse(it.atTime, offsetDateTimeFormatter).toEpochSecond()
                    )
                }

            Rating(
                symbol,
                redditDate,
                redditEntry.score.toDouble(),
                twitterEntry?.score?.toDouble(),
                (twitterEntry?.let { (it.score + redditEntry.score) / 2.0 } ?: redditEntry.score).toDouble()
            )
        }
    }
}
