package de.unigoettingen.tradinghero.rating

data class RatingView(
    val placement: Int,
    val stock: String
)
