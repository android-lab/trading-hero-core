package de.unigoettingen.tradinghero.rating

import de.unigoettingen.trading_hero_core.jooq.tables.Course.COURSE
import de.unigoettingen.trading_hero_core.jooq.tables.Rating.RATING
import de.unigoettingen.trading_hero_core.jooq.tables.Stock.STOCK
import de.unigoettingen.trading_hero_core.jooq.tables.pojos.Course
import de.unigoettingen.trading_hero_core.jooq.tables.pojos.Rating
import de.unigoettingen.trading_hero_core.jooq.tables.pojos.Stock
import de.unigoettingen.tradinghero.database.Jooq
import de.unigoettingen.tradinghero.utils.DBHelper
import org.jooq.impl.DSL.*
import javax.enterprise.context.ApplicationScoped
import javax.inject.Inject

@ApplicationScoped
class RatingRepository {
    @Inject
    lateinit var jooq: Jooq

    fun getTopPrices(amount: Int): List<Course> = jooq.dsl { db ->
        db.selectFrom(COURSE)
            .where(
                row(COURSE.SYMBOL, COURSE.TIMESTAMP).`in`(
                    db.select(COURSE.SYMBOL, max(COURSE.TIMESTAMP))
                        .from(COURSE)
                        .groupBy(COURSE.SYMBOL)
                )
            )
            .orderBy(COURSE.PRICE.desc())
            .limit(amount)
            .fetchInto(Course::class.java)
    }

    fun getTopMarketCap(amount: Int): List<Stock> = jooq.dsl { db ->
        db.selectFrom(STOCK)
            .orderBy(STOCK.MARKETCAPITALIZATION.desc())
            .limit(amount)
            .fetchInto(Stock::class.java)
    }

    fun getTopSocial(amount: Int): MutableList<Rating> = jooq.dsl { db ->
        db.selectFrom(RATING)
            .where(
                row(RATING.STOCK, RATING.DATE).`in`(
                    db.select(RATING.STOCK, max(RATING.DATE))
                        .from(RATING)
                        .groupBy(RATING.STOCK)
                )
            )
            .orderBy(RATING.RATING_.desc())
            .limit(amount)
            .fetchInto(Rating::class.java)
    }

    fun getSocialPlacement(stock: String): Long? = jooq.dsl { db ->
        db.select(count())
            .from(RATING)
            .where(
                row(RATING.STOCK, RATING.DATE).`in`(
                    db.select(RATING.STOCK, max(RATING.DATE))
                        .from(RATING)
                        .groupBy(RATING.STOCK)
                ).and(
                    RATING.RATING_.ge(
                        db.select(RATING.RATING_)
                            .from(RATING)
                            .where(RATING.STOCK.eq(stock))
                            .and(
                                row(RATING.STOCK, RATING.DATE).`in`(
                                    db.select(RATING.STOCK, max(RATING.DATE))
                                        .from(RATING)
                                        .groupBy(RATING.STOCK)
                                )
                            )
                    )
                )
            ).fetchOneInto(Long::class.java)
    }

    fun getPricePlacement(symbol: String): Long? = jooq.dsl { db ->
        db.select(count())
            .from(COURSE)
            .where(
                row(COURSE.SYMBOL, COURSE.TIMESTAMP).`in`(
                    db.select(COURSE.SYMBOL, max(COURSE.TIMESTAMP))
                        .from(COURSE)
                        .groupBy(COURSE.SYMBOL)
                )
            ).and(
                COURSE.PRICE.ge(
                    db.select(COURSE.PRICE)
                        .from(COURSE)
                        .where(COURSE.SYMBOL.eq(symbol))
                        .and(
                            row(COURSE.SYMBOL, COURSE.TIMESTAMP).`in`(
                                db.select(COURSE.SYMBOL, max(COURSE.TIMESTAMP))
                                    .from(COURSE)
                                    .groupBy(COURSE.SYMBOL)
                            )
                        )
                )
            ).fetchOneInto(Long::class.java)
    }

    fun getMarketCapPlacement(symbol: String): Long? = jooq.dsl { db ->
        db.select(count())
            .from(STOCK)
            .where(
                STOCK.MARKETCAPITALIZATION.ge(
                    db.select(STOCK.MARKETCAPITALIZATION)
                        .from(STOCK)
                        .where(STOCK.SYMBOL.eq(symbol))
                )
            ).fetchOneInto(Long::class.java)
    }

    fun updateSocialRatings(ratings: List<Rating>) = jooq.dsl { db ->
        ratings.forEach {
            db.insertInto(RATING)
                .set(DBHelper.listProperties(it))
                .onConflict()
                .doUpdate()
                .set(DBHelper.listProperties(it))
                .execute()
        }
    }
}
