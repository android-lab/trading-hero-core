package de.unigoettingen.tradinghero.rating

enum class RatingType {
    SOCIAL,
    PRICE,
    MARKET_CAP
}
