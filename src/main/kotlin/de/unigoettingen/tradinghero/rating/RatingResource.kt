package de.unigoettingen.tradinghero.rating

import javax.inject.Inject
import javax.ws.rs.*
import javax.ws.rs.core.MediaType

@Path("/rating")
@Produces(MediaType.APPLICATION_JSON)
class RatingResource {
    @Inject
    lateinit var ratingService: RatingService

    @GET
    @Path("/{type}/{symbol}")
    fun getPosition(
        @PathParam("type") type: RatingType,
        @PathParam("symbol") symbol: String
    ) = ratingService.getPlacement(type, symbol)

    @GET
    @Path("/top/{type}")
    fun getTop(
        @PathParam("type") type: RatingType,
        @QueryParam("amount") amount: Int?
    ) = ratingService.getTop(type, amount)
}
