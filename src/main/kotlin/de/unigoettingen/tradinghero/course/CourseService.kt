package de.unigoettingen.tradinghero.course

import de.unigoettingen.trading_hero_core.jooq.tables.pojos.Course
import de.unigoettingen.tradinghero.currency.CurrencyService
import de.unigoettingen.tradinghero.datasource.finnhub.FinnhubRestClient
import de.unigoettingen.tradinghero.datasource.finnhub.FinnhubService
import de.unigoettingen.tradinghero.stocks.StockPreset
import de.unigoettingen.tradinghero.utils.Configuration
import io.quarkus.scheduler.Scheduled
import org.eclipse.microprofile.config.inject.ConfigProperty
import java.lang.Long.min
import java.util.concurrent.TimeUnit
import javax.enterprise.context.ApplicationScoped
import javax.enterprise.inject.Instance
import javax.inject.Inject

@ApplicationScoped
class CourseService {
    @Inject
    lateinit var finnhubService: FinnhubService

    @Inject
    lateinit var stockPreset: StockPreset

    @Inject
    lateinit var finnhubRestClient: FinnhubRestClient

    @Inject
    lateinit var courseRepository: CourseRepository

    @Inject
    lateinit var currencyService: CurrencyService

    @Inject
    lateinit var configuration: Configuration

    @ConfigProperty(name = "trading.hero.defaults.resolution.limit")
    lateinit var defaultLimit: Instance<Int>

    @Scheduled(every = "5s", concurrentExecution = Scheduled.ConcurrentExecution.SKIP)
    fun updateStocks() {
        if (finnhubService.futureAPICalls.size > 5) { // Too many api-calls in queue
            return
        }

        stockPreset.names.forEach { name ->
            val from =
                courseRepository.getLastStockUpdate(name) ?: (System.currentTimeMillis() - TimeUnit.DAYS.toMillis(365))

            var to = min(System.currentTimeMillis(), from + 2591958000) // Caused by max time range of 2591958 seconds
            var candle = finnhubService.scheduleAPICall {
                finnhubRestClient.getStockCourse(
                    name,
                    TimeUnit.MILLISECONDS.toSeconds(from),
                    TimeUnit.MILLISECONDS.toSeconds(to)
                )
            }

            if (candle.c == null || candle.t == null) {
                to = System.currentTimeMillis() // finnhub limits the responded data with the date of "to"
                candle = finnhubService.scheduleAPICall {
                    finnhubRestClient.getStockCourse(
                        name,
                        TimeUnit.MILLISECONDS.toSeconds(from),
                        TimeUnit.MILLISECONDS.toSeconds(to)
                    )
                }
            }

            if (candle.c == null || candle.t == null) {
                println("Error: Failed to fetch stock $name")
                return@forEach
            }
            courseRepository.updateStock(candle.toStockUpdateViews(name))
        }
    }

    fun getCourse(
        symbol: String,
        greaterDiffFromNow: Long?,
        lesserDiffFromNow: Long?,
        limit: Int?,
        currency: String?
    ): List<Course> {
        val amount = (limit ?: defaultLimit.get()) - 1
        val lastCourseUpdate = courseRepository.getLastStockUpdate(symbol) ?: System.currentTimeMillis()

        val courses = courseRepository.getStockUpdates(
            symbol,
            greaterDiffFromNow?.let { lastCourseUpdate - it },
            lesserDiffFromNow?.let { lastCourseUpdate - it },
            amount
        )

        return courses.onEach {
            it.price = currencyService.toCurrency(
                from = configuration.baseCurrency.get(),
                to = currency ?: configuration.baseCurrency.get(),
                amount = it.price
            )
        }
    }

    fun getLastStockPrice(symbol: String, currency: String?) =
        courseRepository.getLastStockPrice(symbol)?.apply {
            price = currencyService.toCurrency(
                from = configuration.baseCurrency.get(),
                to = currency ?: configuration.baseCurrency.get(),
                amount = price
            )
        }

    fun getLastStockPrices(symbols: List<String>, currency: String?) =
        symbols.map { courseRepository.getLastStockPrice(it) }.onEach {
            if (it != null) {
                it.price = currencyService.toCurrency(
                    from = configuration.baseCurrency.get(),
                    to = currency ?: configuration.baseCurrency.get(),
                    amount = it.price
                )
            }
        }
}
