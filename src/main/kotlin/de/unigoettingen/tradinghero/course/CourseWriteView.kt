package de.unigoettingen.tradinghero.course

data class CourseWriteView(
    val symbol: String,
    val price: Double,
    val timeStamp: Long
)
