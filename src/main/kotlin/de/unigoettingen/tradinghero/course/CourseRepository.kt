package de.unigoettingen.tradinghero.course

import de.unigoettingen.trading_hero_core.jooq.tables.Course.COURSE
import de.unigoettingen.trading_hero_core.jooq.tables.pojos.Course
import de.unigoettingen.tradinghero.database.Jooq
import org.jooq.impl.DSL.*
import javax.enterprise.context.ApplicationScoped
import javax.inject.Inject

@ApplicationScoped
class CourseRepository {
    @set:Inject
    lateinit var jooq: Jooq

    fun updateStock(courseWriteViews: List<CourseWriteView>, sync: Boolean = false) {
        jooq.dsl { db ->
            courseWriteViews.forEach {
                with(it) {
                    val query = db.insertInto(COURSE)
                        .values(this.symbol, this.timeStamp, this.price)
                        .onConflictDoNothing()

                    if (sync) {
                        query.execute()
                    } else {
                        query.executeAsync()
                    }
                }
            }
        }
    }

    fun getLastStockUpdate(symbol: String): Long? {
        return jooq.dsl { db ->
            db.select(max(COURSE.TIMESTAMP))
                .from(COURSE)
                .where(COURSE.SYMBOL.eq(symbol))
                .fetchOne(max(COURSE.TIMESTAMP))
        }
    }

    fun getStockUpdates(symbol: String, from: Long?, to: Long?, amount: Int): List<Course> {
        val conditions = and(
            listOfNotNull(
                COURSE.SYMBOL.eq(symbol),
                from?.let { COURSE.TIMESTAMP.ge(it) },
                to?.let { COURSE.TIMESTAMP.le(it) }
            )
        )

        return jooq.dsl { db ->
            db.select(COURSE.SYMBOL, COURSE.TIMESTAMP, COURSE.PRICE)
                .from(COURSE)
                .join(
                    db.select(COURSE.TIMESTAMP, rowNumber().over().orderBy(COURSE.TIMESTAMP).`as`("row"))
                        .from(COURSE)
                        .where(conditions).asTable("t1")
                )
                .on(field("t1.timestamp").eq(COURSE.TIMESTAMP))
                .crossJoin(
                    db.select(count().`as`("amount"))
                        .from(COURSE)
                        .where(conditions)
                )
                .where(conditions)
                .and(
                    field("t1.row").mod(
                        greatest(
                            field("amount").cast(Int::class.java),
                            amount.coerceAtLeast(1)
                        ).cast(Int::class.java)
                            .div(amount.coerceAtLeast(1))
                    ).eq(0)
                )
        }.fetchInto(Course::class.java)
    }

    fun getLastStockPrice(symbol: String): Course? {
        return jooq.dsl { db ->
            db.selectFrom(COURSE).where(
                COURSE.SYMBOL.eq(symbol).and(
                    COURSE.TIMESTAMP.eq(
                        db.select(max(COURSE.TIMESTAMP))
                            .from(COURSE)
                            .where(COURSE.SYMBOL.eq(symbol))
                            .fetchOneInto(Long::class.java)
                    )
                )
            ).fetchOneInto(Course::class.java)
        }
    }
}
