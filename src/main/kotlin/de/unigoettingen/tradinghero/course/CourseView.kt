package de.unigoettingen.tradinghero.course

data class CourseView(
    val symbol: String,
    val timeStamp: Long,
    val price: Double
)
