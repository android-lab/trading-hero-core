package de.unigoettingen.tradinghero.persons

import de.unigoettingen.trading_hero_core.jooq.tables.pojos.Person
import de.unigoettingen.tradinghero.balance.BalanceService
import javax.enterprise.context.ApplicationScoped
import javax.inject.Inject

@ApplicationScoped
class PersonService {

    @Inject
    lateinit var personRepository: PersonRepository

    @Inject
    lateinit var balanceService: BalanceService

    fun createPerson(person: Person): Person? {
        val dbPerson = personRepository.createPerson(person)
        balanceService.createStarterBalance(person.id)
        return dbPerson
    }

    fun existPerson(personId: String) =
        personRepository.existPerson(personId)

    fun getPerson(personId: String) =
        personRepository.getPerson(personId)

    fun getLimitedPersonData(personId: String) =
        personRepository.getLimitedPersonData(personId)

    fun updatePerson(personId: String, personWriteView: PersonWriteView) =
        personRepository.updatePerson(personId, personWriteView)
}
