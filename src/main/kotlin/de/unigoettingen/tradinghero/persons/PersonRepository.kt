package de.unigoettingen.tradinghero.persons

import de.unigoettingen.trading_hero_core.jooq.tables.Person.PERSON
import de.unigoettingen.trading_hero_core.jooq.tables.pojos.Person
import de.unigoettingen.tradinghero.database.Jooq
import de.unigoettingen.tradinghero.utils.DBHelper
import javax.enterprise.context.ApplicationScoped
import javax.inject.Inject

@ApplicationScoped
class PersonRepository {

    @set:Inject
    lateinit var jooq: Jooq

    fun createPerson(person: Person): Person? =
        jooq.dsl { db ->
            db.insertInto(PERSON)
                .set(DBHelper.listProperties(person))
                .returning()
                .fetchOneInto(Person::class.java)
        }

    fun updatePerson(personId: String, personWriteView: PersonWriteView): Person? {
        return with(personWriteView) {
            val updateMap = mutableMapOf(
                PERSON.FAMILY_NAME to familyName,
                PERSON.GIVEN_NAME to givenName,
                PERSON.NAME to name,
                PERSON.IMAGE to image
            )
            jooq.dsl { db ->
                db.update(PERSON).set(updateMap)
                    .where(PERSON.ID.eq(personId))
                    .returning()
                    .fetchOneInto(Person::class.java)
            }
        }
    }

    fun getLimitedPersonData(id: String): Person? =
        jooq.dsl { db ->
            db.select(PERSON.ID, PERSON.NAME, PERSON.IMAGE)
                .from(PERSON)
                .where(PERSON.ID.eq(id))
                .fetchOneInto(Person::class.java)
        }

    fun getPerson(id: String): Person? =
        jooq.dsl { db ->
            db.selectFrom(PERSON)
                .where(PERSON.ID.eq(id))
                .fetchOneInto(Person::class.java)
        }

    fun existPerson(id: String): Boolean =
        jooq.dsl { db ->
            db.fetchExists(PERSON, PERSON.ID.eq(id))
        }

    fun removePerson(id: String) =
        jooq.dsl { db ->
            db.deleteFrom(PERSON).where(PERSON.ID.eq(id)).execute()
        }
}
