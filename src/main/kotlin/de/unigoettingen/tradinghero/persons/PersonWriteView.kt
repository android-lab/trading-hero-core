package de.unigoettingen.tradinghero.persons

data class PersonWriteView(
    val name: String,
    val image: String,
    val familyName: String,
    val givenName: String,
) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as PersonWriteView

        if (name != other.name) return false
        if (image != other.image) return false
        if (familyName != other.familyName) return false
        if (givenName != other.givenName) return false

        return true
    }

    override fun hashCode(): Int {
        var result = name.hashCode()
        result = 31 * result + image.hashCode()
        result = 31 * result + familyName.hashCode()
        result = 31 * result + givenName.hashCode()
        return result
    }
}
