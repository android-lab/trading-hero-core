package de.unigoettingen.tradinghero.persons

import java.time.OffsetDateTime

data class PersonView(
    val id: String,
    val name: String,
    val email: String?,
    val image: String,
    val familyName: String?,
    val givenName: String?,
    val dateCreated: OffsetDateTime?
)
