package de.unigoettingen.tradinghero.persons

import de.unigoettingen.trading_hero_core.jooq.tables.pojos.Person
import de.unigoettingen.tradinghero.auth.getUser
import de.unigoettingen.tradinghero.auth.getUserId
import java.time.OffsetDateTime.now
import javax.inject.Inject
import javax.ws.rs.*
import javax.ws.rs.core.Context
import javax.ws.rs.core.MediaType
import javax.ws.rs.core.SecurityContext

@Path("/persons")
@Produces(MediaType.APPLICATION_JSON)
class PersonResource {
    @Inject
    lateinit var personService: PersonService

    @Context
    lateinit var ctx: SecurityContext

    @GET
    @Path("/me")
    fun me(): PersonView? {
        return if (personService.existPerson(getUserId(ctx))) {
            personService.getPerson(getUserId(ctx))?.let { toView(it) }
        } else {
            with(getUser(ctx)) {
                val person = Person(id, name, email, image, family_name, given_name, now())
                personService.createPerson(person)?.let { toView(it) }
            }
        }
    }

    @GET
    @Path("/{userId}")
    fun getPerson(@PathParam("userId") userId: String): PersonView? {
        return personService.getLimitedPersonData(userId)?.let { toView(it) }
    }

    @PUT
    @Path("/me")
    fun updatePerson(personWriteView: PersonWriteView): PersonView? {
        return personService.updatePerson(getUserId(ctx), personWriteView)?.let { toView(it) }
    }

    companion object {
        private fun toView(person: Person): PersonView {
            return with(person) {
                PersonView(id, name, email, image, familyName, givenName, dateCreated)
            }
        }
    }
}
