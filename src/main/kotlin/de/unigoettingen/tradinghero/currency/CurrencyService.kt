package de.unigoettingen.tradinghero.currency

import de.unigoettingen.tradinghero.datasource.APICallRequester
import de.unigoettingen.tradinghero.datasource.finnhub.FinnhubRestClient
import de.unigoettingen.tradinghero.datasource.finnhub.FinnhubService
import de.unigoettingen.tradinghero.utils.Configuration
import io.quarkus.scheduler.Scheduled
import javax.enterprise.context.ApplicationScoped
import javax.inject.Inject

@ApplicationScoped
class CurrencyService {
    @Inject
    lateinit var finnhubService: FinnhubService

    @Inject
    lateinit var finnhubRestClient: FinnhubRestClient

    @Inject
    lateinit var configuration: Configuration

    private val currencyMap by lazy {
        mutableMapOf(Pair(configuration.baseCurrency.get(), 1.0))
    }

    @Scheduled(every = "1h")
    fun updateForexRates() {
        val forexRates = finnhubService.scheduleAPICall(
            requester = APICallRequester.STOCK,
            prioritized = true
        ) {
            finnhubRestClient.getForexRates(configuration.baseCurrency.get())
        }
        currencyMap.putAll(forexRates.quote)
    }

    fun toCurrency(from: String, to: String, amount: Double): Double {
        val fromExchangeCourse = currencyMap[from]
        val toExchangeCourse = currencyMap[to]

        if (fromExchangeCourse == null || toExchangeCourse == null) {
            throw IllegalArgumentException("Can't find exchange course for given arguments!")
        }
        val usDollar = amount / fromExchangeCourse
        return usDollar * toExchangeCourse
    }

    fun getListOfCurrencies() = currencyMap.keys
}
