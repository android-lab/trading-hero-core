package de.unigoettingen.tradinghero.currency

import javax.inject.Inject
import javax.ws.rs.GET
import javax.ws.rs.Path
import javax.ws.rs.Produces
import javax.ws.rs.core.MediaType

@Path("/currencies")
@Produces(MediaType.APPLICATION_JSON)
class CurrencyResource {

    @Inject
    lateinit var currencyService: CurrencyService

    @GET
    fun getListOfCurrencies() = currencyService.getListOfCurrencies()
}
