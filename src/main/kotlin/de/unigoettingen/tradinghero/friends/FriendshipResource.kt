package de.unigoettingen.tradinghero.friends

import de.unigoettingen.tradinghero.auth.getUserId
import javax.inject.Inject
import javax.ws.rs.*
import javax.ws.rs.core.Context
import javax.ws.rs.core.MediaType
import javax.ws.rs.core.SecurityContext

@Path("/friendship")
@Produces(MediaType.APPLICATION_JSON)
class FriendshipResource {
    @Inject
    lateinit var friendshipService: FriendshipService

    @Context
    lateinit var ctx: SecurityContext

    @POST
    @Path("{friendId}")
    fun addFriend(@PathParam("friendId") friendId: String): FriendRequestView? {
        return friendshipService.addFriend(getUserId(ctx), friendId)?.let {
            FriendRequestView(it.friendId, it.dateCreated.toInstant().toEpochMilli())
        }
    }

    @DELETE
    @Path("{friendId}")
    fun removeFriend(@PathParam("friendId") friendId: String) {
        friendshipService.removeFriend(getUserId(ctx), friendId)
    }

    @GET
    fun getFriends(): List<FriendShipView> {
        return friendshipService.getFriends(getUserId(ctx))
            .map { FriendShipView(it.personId, it.friendId, it.dateAccepted.toInstant().toEpochMilli()) }
    }

    @GET
    @Path("invites")
    fun getInvites(): List<FriendRequestView> {
        return friendshipService.getFriendInvitations(getUserId(ctx))
            .map { FriendRequestView(it.personId, it.dateCreated.toInstant().toEpochMilli()) }
    }

    @GET
    @Path("requests")
    fun getRequests(): List<FriendRequestView> {
        return friendshipService.getFriendRequests(getUserId(ctx))
            .map { FriendRequestView(it.friendId, it.dateCreated.toInstant().toEpochMilli()) }
    }
}
