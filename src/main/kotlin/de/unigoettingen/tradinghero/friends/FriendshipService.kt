package de.unigoettingen.tradinghero.friends

import de.unigoettingen.trading_hero_core.jooq.tables.pojos.Friendship
import java.time.OffsetDateTime
import javax.enterprise.context.ApplicationScoped
import javax.inject.Inject
import javax.ws.rs.WebApplicationException

@ApplicationScoped
class FriendshipService {
    @set:Inject
    lateinit var friendshipRepository: FriendshipRepository

    fun addFriend(personId: String, friendId: String): Friendship? {
        if (personId == friendId) {
            throw WebApplicationException("You couldn't add yourself as friend", 403)
        }

        val now = OffsetDateTime.now()
        var friendship = Friendship(personId, friendId, null, now)
        friendshipRepository.addFriendship(friendship)?.let {
            if (friendshipRepository.existMutualFriendship(personId, friendId)) {
                friendshipRepository.updateAcceptedDate(friendId, personId, now)
                friendshipRepository.updateAcceptedDate(personId, friendId, now)?.let {
                    friendship = it
                }
            }
        }
        return friendship
    }

    // Get all the Friendships that are accepted, from my perspective
    fun getFriends(personId: String): List<Friendship> =
        friendshipRepository.getFriendShips(personId).filter { it.personId == personId && it.dateAccepted != null }

    // Get all the Friendships that I initiated, but which are not accepted yet
    fun getFriendRequests(personId: String): List<Friendship> =
        friendshipRepository.getFriendShips(personId).filter { it.personId == personId && it.dateAccepted == null }

    // Get all the Friendships that someone else initiated, but which I have not accepted yet
    fun getFriendInvitations(personId: String): List<Friendship> =
        friendshipRepository.getFriendShips(personId).filter { it.friendId == personId && it.dateAccepted == null }

    fun removeFriend(personId: String, friendId: String): Int {
        return friendshipRepository.removeFriendship(personId, friendId) +
            friendshipRepository.removeFriendship(friendId, personId)
    }

    fun areFriends(userId: String, friendId: String) = friendshipRepository.existMutualFriendship(userId, friendId)
}
