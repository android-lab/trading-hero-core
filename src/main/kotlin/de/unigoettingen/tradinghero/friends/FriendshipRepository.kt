package de.unigoettingen.tradinghero.friends

import de.unigoettingen.trading_hero_core.jooq.Tables.FRIENDSHIP
import de.unigoettingen.trading_hero_core.jooq.tables.pojos.Friendship
import de.unigoettingen.tradinghero.database.Jooq
import de.unigoettingen.tradinghero.utils.DBHelper
import java.time.OffsetDateTime
import javax.enterprise.context.ApplicationScoped
import javax.inject.Inject

@ApplicationScoped
class FriendshipRepository {

    @set:Inject
    lateinit var jooq: Jooq

    fun addFriendship(friendship: Friendship): Friendship? = jooq.dsl { db ->
        db.insertInto(FRIENDSHIP)
            .set(DBHelper.listProperties(friendship))
            .returning()
            .fetchOneInto(Friendship::class.java)
    }

    fun updateAcceptedDate(personId: String, friendId: String, acceptedDate: OffsetDateTime): Friendship? {
        val updateMap = mutableMapOf(
            FRIENDSHIP.DATE_ACCEPTED to acceptedDate
        )
        return jooq.dsl { db ->
            db.update(FRIENDSHIP).set(updateMap)
                .where(FRIENDSHIP.PERSON_ID.eq(personId).and(FRIENDSHIP.FRIEND_ID.eq(friendId)))
                .returning()
                .fetchOneInto(Friendship::class.java)
        }
    }

    fun getFriendShip(personId: String, friendId: String): Friendship? =
        jooq.dsl { db ->
            db.selectFrom(FRIENDSHIP)
                .where(FRIENDSHIP.PERSON_ID.eq(personId).and(FRIENDSHIP.FRIEND_ID.eq(friendId)))
                .fetchOneInto(Friendship::class.java)
        }

    // Returns a complete list of friendships (Both sides, [you -> friend] and [friend -> you])
    fun getFriendShips(personId: String): List<Friendship> =
        jooq.dsl { db ->
            db.selectFrom(FRIENDSHIP)
                .where(FRIENDSHIP.PERSON_ID.eq(personId).or(FRIENDSHIP.FRIEND_ID.eq(personId)))
                .fetchInto(Friendship::class.java)
        }

    fun existMutualFriendship(personId: String, otherPersonId: String): Boolean =
        jooq.dsl { db ->
            db.fetchExists(
                db.selectFrom(FRIENDSHIP)
                    .where(FRIENDSHIP.PERSON_ID.eq(personId).and(FRIENDSHIP.FRIEND_ID.eq(otherPersonId)))
            ) && db.fetchExists(
                db.selectFrom(FRIENDSHIP)
                    .where(FRIENDSHIP.PERSON_ID.eq(otherPersonId).and(FRIENDSHIP.FRIEND_ID.eq(personId)))
            )
        }

    fun removeFriendship(personId: String, friendId: String) =
        jooq.dsl { db ->
            db.deleteFrom(FRIENDSHIP).where(
                FRIENDSHIP.PERSON_ID.eq(personId).and(FRIENDSHIP.FRIEND_ID.eq(friendId))
                    .or(FRIENDSHIP.PERSON_ID.eq(friendId).and(FRIENDSHIP.FRIEND_ID.eq(personId)))
            ).execute()
        }
}
