package de.unigoettingen.tradinghero.friends

data class FriendShipView(
    val personId: String,
    val friendId: String,
    val dateAccepted: Long,
)

data class FriendRequestView(
    val friendId: String,
    val dateCreated: Long
)
