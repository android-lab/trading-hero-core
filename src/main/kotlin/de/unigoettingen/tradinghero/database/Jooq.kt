package de.unigoettingen.tradinghero.database

import org.jooq.Condition
import org.jooq.DSLContext
import org.jooq.Field
import org.jooq.SQLDialect
import org.jooq.impl.DSL
import javax.enterprise.context.ApplicationScoped
import javax.inject.Inject
import javax.sql.DataSource

@ApplicationScoped
class Jooq {

    @set:Inject
    lateinit var dataSource: DataSource

    fun <R> dsl(function: (DSLContext) -> R): R {
        try {
            return DSL.using(dataSource, SQLDialect.POSTGRES).run(function)
        } catch (e: Exception) {
            e.printStackTrace()
            throw e
        }
    }

    companion object {
        fun distance(field: Field<String>, searchString: String): Field<Any> {
            return DSL.field("{0} <-> {1}", field, searchString)
        }

        fun similar(a: Field<String>, searchString: String): Condition {
            return DSL.condition("{0} % {1}", a, searchString)
        }
    }
}
