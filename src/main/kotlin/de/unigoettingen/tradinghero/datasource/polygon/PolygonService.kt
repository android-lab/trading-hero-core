package de.unigoettingen.tradinghero.datasource.polygon

import de.unigoettingen.tradinghero.datasource.ScheduleService
import io.quarkus.scheduler.Scheduled
import javax.enterprise.context.ApplicationScoped
import kotlin.concurrent.withLock

@ApplicationScoped
class PolygonService : ScheduleService("polygon") {
    @Scheduled(every = "12s", delay = 0)
    fun scheduleAPICalls() {
        val element = futureAPICalls.firstOrNull { blockingRequester == null || blockingRequester == it.requester }
            ?: return

        lock.withLock {
            futureAPICalls.remove(element)
            element.condition.signal() // free blocked thread
        }
    }
}
