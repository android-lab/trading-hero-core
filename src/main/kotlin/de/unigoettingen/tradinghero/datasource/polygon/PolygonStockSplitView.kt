package de.unigoettingen.tradinghero.datasource.polygon

data class PolygonStockSplitView(
    val count: Int,
    val results: List<PolygonStockSplitEntry>
)

data class PolygonStockSplitEntry(
    val ticker: String,
    val exDate: String,
    val paymentDate: String,
    val declaredDate: String?,
    val ratio: Float,
    val tofactor: Int?,
    val forfactor: Int?
)
