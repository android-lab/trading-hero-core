package de.unigoettingen.tradinghero.datasource.polygon

import de.unigoettingen.tradinghero.utils.Configuration
import org.eclipse.microprofile.rest.client.inject.RegisterRestClient
import org.eclipse.microprofile.rest.client.inject.RestClient
import javax.enterprise.context.ApplicationScoped
import javax.inject.Inject
import javax.ws.rs.GET
import javax.ws.rs.Path
import javax.ws.rs.PathParam
import javax.ws.rs.QueryParam

@Path("/v2")
@RegisterRestClient
interface PolygonRestClientInterface {
    @GET
    @Path("/reference/splits/{symbol}")
    fun getStockSplits(
        @PathParam("symbol") symbol: String,
        @QueryParam("apiKey") apiKey: String,
    ): PolygonStockSplitView
}

@ApplicationScoped
class PolygonRestClient {
    @Inject
    lateinit var configuration: Configuration

    @Inject
    @RestClient
    lateinit var polygonRestClientInterface: PolygonRestClientInterface

    fun getStockSplits(symbol: String) =
        polygonRestClientInterface.getStockSplits(symbol, configuration.polygonToken.get())
}
