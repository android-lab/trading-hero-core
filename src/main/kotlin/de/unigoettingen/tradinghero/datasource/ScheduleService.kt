package de.unigoettingen.tradinghero.datasource

import java.util.concurrent.CopyOnWriteArrayList
import java.util.concurrent.locks.ReentrantLock
import javax.net.ssl.SSLException
import javax.ws.rs.WebApplicationException
import kotlin.concurrent.withLock

open class ScheduleService(open val dataSource: String) {
    open val lock = ReentrantLock()
    open val futureAPICalls = CopyOnWriteArrayList<APICall>()
    open var blockingRequester: APICallRequester? = null // null equals free state

    open fun <T> scheduleAPICall(
        requester: APICallRequester? = null,
        triesLeft: Int = 60,
        prioritized: Boolean = false,
        function: () -> T
    ): T {
        val apiCall = APICall(lock.newCondition(), requester)

        lock.withLock {
            if (prioritized) {
                futureAPICalls.add(0, apiCall)
            } else {
                futureAPICalls.add(apiCall)
            }
            apiCall.condition.await() // block current thread and wait until the task is ready to run
        }

        return try {
            function()
        } catch (exception: WebApplicationException) { // Too many requests caused by finnhub rate limit
            if (triesLeft <= 0 || exception.response.status != 429) {
                throw exception
            }

            println("Failed to execute $dataSource-request: Tries left: #$triesLeft")
            scheduleAPICall(
                requester = requester,
                triesLeft = triesLeft - 1,
                prioritized = true,
                function = function
            )
        } catch (exception: SSLException) { // SSL-Handshake failed
            if (triesLeft <= 0) {
                throw exception
            }

            scheduleAPICall(
                requester = requester,
                triesLeft = triesLeft - 1,
                prioritized = true,
                function = function
            )
        }
    }

    open fun block(requester: APICallRequester) {
        this.blockingRequester = requester
    }

    open fun free() {
        this.blockingRequester = null
    }

    open fun getAPICallCount() = futureAPICalls.size
}
