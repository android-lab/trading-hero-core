package de.unigoettingen.tradinghero.datasource

import java.util.concurrent.locks.Condition

data class APICall(
    val condition: Condition,
    val requester: APICallRequester?,
)
