package de.unigoettingen.tradinghero.datasource

enum class APICallRequester {
    STOCK,
    COURSE
}
