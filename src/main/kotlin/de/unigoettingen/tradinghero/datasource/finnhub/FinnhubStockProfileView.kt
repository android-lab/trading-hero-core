package de.unigoettingen.tradinghero.datasource.finnhub

import com.fasterxml.jackson.annotation.JsonIgnoreProperties

@JsonIgnoreProperties(ignoreUnknown = true)
data class FinnhubStockProfileView(
    var exchange: String,
    var currency: String,
    var finnhubIndustry: String,
    var ipo: String,
    var logo: String,
    var marketCapitalization: Double,
    var name: String,
    var shareOutstanding: Double,
    var weburl: String
)
