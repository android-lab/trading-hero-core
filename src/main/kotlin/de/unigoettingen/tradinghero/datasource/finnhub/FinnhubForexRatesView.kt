package de.unigoettingen.tradinghero.datasource.finnhub

data class FinnhubForexRatesView(
    val base: String,
    val quote: Map<String, Double>
)
