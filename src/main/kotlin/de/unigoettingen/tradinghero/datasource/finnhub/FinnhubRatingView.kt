package de.unigoettingen.tradinghero.datasource.finnhub

data class FinnhubRatingView(
    val reddit: List<FinnhubRatingViewEntry>,
    val symbol: String,
    val twitter: List<FinnhubRatingViewEntry>
)

data class FinnhubRatingViewEntry(
    val atTime: String,
    val mention: Int,
    val positiveScore: Float,
    val negativeScore: Float,
    val positiveMention: Int,
    val negativeMention: Int,
    val score: Float
)
