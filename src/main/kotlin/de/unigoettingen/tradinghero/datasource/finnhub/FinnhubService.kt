package de.unigoettingen.tradinghero.datasource.finnhub

import de.unigoettingen.tradinghero.datasource.ScheduleService
import io.quarkus.scheduler.Scheduled
import javax.enterprise.context.ApplicationScoped
import kotlin.concurrent.withLock

@ApplicationScoped
class FinnhubService : ScheduleService("finnhub") {
    @Scheduled(every = "2s", delay = 0)
    fun scheduleAPICalls() {
        val element = futureAPICalls.firstOrNull { blockingRequester == null || blockingRequester == it.requester }
            ?: return

        lock.withLock {
            futureAPICalls.remove(element)
            element.condition.signal() // free blocked thread
        }
    }
}
