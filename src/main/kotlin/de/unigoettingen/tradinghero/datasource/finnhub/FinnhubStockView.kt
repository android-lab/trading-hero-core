package de.unigoettingen.tradinghero.datasource.finnhub

import de.unigoettingen.trading_hero_core.jooq.tables.pojos.Stock
import java.sql.Date

data class FinnhubStockView(
    val symbol: String,
    val currency: String,
    val description: String,
    val figi: String,
    val mic: String,
    val displaySymbol: String,
    val type: String
)

fun toStock(stock: FinnhubStockView): Stock {
    return with(stock) {
        Stock(
            symbol,
            currency,
            "",
            description,
            figi,
            mic,
            displaySymbol,
            type,
            "",
            "",
            Date(System.currentTimeMillis()),
            "",
            0.0,
            0.0,
            ""
        )
    }
}
