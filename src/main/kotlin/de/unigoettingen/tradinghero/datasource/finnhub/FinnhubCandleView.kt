package de.unigoettingen.tradinghero.datasource.finnhub

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import de.unigoettingen.tradinghero.course.CourseWriteView
import java.util.concurrent.TimeUnit

@JsonIgnoreProperties(ignoreUnknown = true)
data class FinnhubCandleView(
    val c: MutableList<Double>?,
    val t: MutableList<Long>?,
) {
    fun toStockUpdateViews(symbol: String): List<CourseWriteView> {
        if (c == null || t == null)
            return emptyList()

        val timeStampIterator = t.iterator()

        return c.map {
            CourseWriteView(symbol, it, TimeUnit.SECONDS.toMillis(timeStampIterator.next()))
        }
    }
}
