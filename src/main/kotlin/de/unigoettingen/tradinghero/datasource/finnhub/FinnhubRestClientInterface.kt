package de.unigoettingen.tradinghero.datasource.finnhub

import de.unigoettingen.tradinghero.utils.Configuration
import org.eclipse.microprofile.rest.client.inject.RegisterRestClient
import org.eclipse.microprofile.rest.client.inject.RestClient
import javax.enterprise.context.ApplicationScoped
import javax.inject.Inject
import javax.ws.rs.GET
import javax.ws.rs.Path
import javax.ws.rs.QueryParam

@Path("/v1")
@RegisterRestClient
interface FinnhubRestClientInterface {
    @GET
    @Path("/stock/symbol")
    fun getAllStocks(
        @QueryParam("exchange") exchange: String,
        @QueryParam("token") token: String
    ): List<FinnhubStockView>

    @GET
    @Path("/stock/candle")
    fun getStockCourse(
        @QueryParam("symbol") symbol: String,
        @QueryParam("from") from: Long,
        @QueryParam("to") to: Long,
        @QueryParam("resolution") resolution: String,
        @QueryParam("token") token: String
    ): FinnhubCandleView

    @GET
    @Path("/stock/profile2")
    fun getProfile(
        @QueryParam("symbol") symbol: String,
        @QueryParam("token") token: String
    ): FinnhubStockProfileView

    @GET
    @Path("/forex/rates")
    fun getForexRates(
        @QueryParam("base") base: String,
        @QueryParam("token") token: String
    ): FinnhubForexRatesView

    @GET
    @Path("/stock/social-sentiment")
    fun getSocialSentiment(
        @QueryParam("symbol") symbol: String,
        @QueryParam("token") token: String
    ): FinnhubRatingView
}

@ApplicationScoped
class FinnhubRestClient {
    @Inject
    lateinit var configuration: Configuration

    @Inject
    @RestClient
    lateinit var finnhubRestClient: FinnhubRestClientInterface

    fun getAllStocks(exchange: String) = finnhubRestClient.getAllStocks(exchange, configuration.finnhubToken.get())

    fun getStockCourse(
        symbol: String,
        from: Long,
        to: Long,
    ) = finnhubRestClient.getStockCourse(symbol, from, to, "1", configuration.finnhubToken.get())

    fun getProfile(
        symbol: String,
    ) = finnhubRestClient.getProfile(symbol, configuration.finnhubToken.get())

    fun getForexRates(base: String) = finnhubRestClient.getForexRates(base, configuration.finnhubToken.get())

    fun getSocialSentiment(symbol: String) = finnhubRestClient.getSocialSentiment(
        symbol,
        configuration.finnhubToken.get()
    )
}
