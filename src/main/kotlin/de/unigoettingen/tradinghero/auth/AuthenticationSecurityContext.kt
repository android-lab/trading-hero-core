package de.unigoettingen.tradinghero.auth

import java.security.Principal
import javax.ws.rs.core.SecurityContext
import javax.ws.rs.core.UriInfo

class AuthenticationSecurityContext(private val person: PersonPrincipal, private val uriInfo: UriInfo) :
    SecurityContext {
    override fun getUserPrincipal(): Principal {
        return person
    }

    override fun isUserInRole(role: String?): Boolean {
        return true
    }

    override fun isSecure(): Boolean {
        return uriInfo.absolutePath.toString().startsWith("https")
    }

    override fun getAuthenticationScheme(): String {
        return SecurityContext.BASIC_AUTH
    }
}
