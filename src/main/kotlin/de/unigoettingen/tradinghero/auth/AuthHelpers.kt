package de.unigoettingen.tradinghero.auth

import de.unigoettingen.tradinghero.exceptions.AuthenticationException
import javax.ws.rs.core.SecurityContext

fun getUserId(ctx: SecurityContext): String {
    return if (ctx.userPrincipal is PersonPrincipal) {
        (ctx.userPrincipal as PersonPrincipal).id
    } else {
        throw AuthenticationException("UserPrinciple is not of class PersonPrincipal")
    }
}

fun getUser(ctx: SecurityContext): PersonPrincipal {
    return if (ctx.userPrincipal is PersonPrincipal) {
        (ctx.userPrincipal as PersonPrincipal)
    } else {
        throw AuthenticationException("UserPrinciple is not of class PersonPrincipal")
    }
}
