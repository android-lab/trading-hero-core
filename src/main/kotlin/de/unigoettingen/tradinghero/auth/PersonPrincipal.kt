package de.unigoettingen.tradinghero.auth

import java.security.Principal

data class PersonPrincipal(
    val id: String,
    private val name: String,
    val email: String,
    val image: String,
    val locale: String,
    val family_name: String,
    val given_name: String
) : Principal {
    override fun getName(): String {
        return this.name
    }
}
