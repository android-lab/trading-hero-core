package de.unigoettingen.tradinghero.auth

import com.google.api.client.googleapis.auth.oauth2.GoogleIdToken
import com.google.api.client.googleapis.auth.oauth2.GoogleIdTokenVerifier
import com.google.api.client.http.javanet.NetHttpTransport
import com.google.api.client.json.gson.GsonFactory
import java.util.Collections

class GoogleAuthenticationTokenVerifier(clientId: String) {
    private val transport = NetHttpTransport()
    private val jsonFactory = GsonFactory.getDefaultInstance()
    private val verifier = GoogleIdTokenVerifier.Builder(transport, jsonFactory)
        .setAudience(Collections.singleton(clientId))
        .build()

    fun getPerson(token: String?): PersonPrincipal? {
        token ?: return null

        val idToken = getVerifiedToken(token) ?: return null
        val payload = idToken.payload

        return PersonPrincipal(
            payload.subject,
            payload["name"] as String,
            payload.email,
            payload["picture"] as String,
            payload["locale"] as String,
            payload["family_name"] as String,
            payload["given_name"] as String
        )
    }

    private fun getVerifiedToken(token: String): GoogleIdToken? {
        return verifier.verify(token)
    }
}
