package de.unigoettingen.tradinghero.auth

import de.unigoettingen.tradinghero.utils.Configuration
import de.unigoettingen.tradinghero.utils.TestDataProvider
import javax.annotation.Priority
import javax.inject.Inject
import javax.ws.rs.Priorities
import javax.ws.rs.container.ContainerRequestContext
import javax.ws.rs.container.ContainerRequestFilter
import javax.ws.rs.container.PreMatching
import javax.ws.rs.core.Context
import javax.ws.rs.core.HttpHeaders
import javax.ws.rs.core.Response
import javax.ws.rs.core.UriInfo
import javax.ws.rs.ext.Provider

@Priority(Priorities.AUTHENTICATION)
@PreMatching
@Provider
class AuthenticationSecurityFilter : ContainerRequestFilter {
    @Context
    lateinit var uriInfo: UriInfo

    private var verifier: GoogleAuthenticationTokenVerifier? = null

    @Inject
    lateinit var configuration: Configuration

    override fun filter(requestContext: ContainerRequestContext?) {
        if (configuration.security.get() == "enabled") {
            this.verifier = this.verifier ?: GoogleAuthenticationTokenVerifier(configuration.clientId.get())
            val person = this.verifier?.getPerson(requestContext?.getHeaderString(HttpHeaders.AUTHORIZATION))
            if (person == null) {
                requestContext?.abortWith(Response.status(Response.Status.UNAUTHORIZED).build())
                return
            }
            requestContext?.securityContext = AuthenticationSecurityContext(
                person, uriInfo
            )
        } else {
            // Serve TestPerson if security is disabled
            requestContext?.securityContext = AuthenticationSecurityContext(
                with(TestDataProvider.person) { PersonPrincipal(id, name, email, image, "de", familyName, givenName) },
                uriInfo
            )
        }
    }
}
