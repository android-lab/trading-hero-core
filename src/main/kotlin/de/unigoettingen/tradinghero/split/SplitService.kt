package de.unigoettingen.tradinghero.split

import de.unigoettingen.trading_hero_core.jooq.tables.pojos.Split
import de.unigoettingen.tradinghero.datasource.APICallRequester
import de.unigoettingen.tradinghero.datasource.finnhub.FinnhubService
import de.unigoettingen.tradinghero.datasource.polygon.PolygonRestClient
import de.unigoettingen.tradinghero.datasource.polygon.PolygonService
import de.unigoettingen.tradinghero.datasource.polygon.PolygonStockSplitEntry
import de.unigoettingen.tradinghero.depot.DepotService
import de.unigoettingen.tradinghero.stocks.StockPreset
import de.unigoettingen.tradinghero.transactions.TransactionService
import de.unigoettingen.tradinghero.transactions.TransactionType
import de.unigoettingen.tradinghero.transactions.TransactionWriteView
import io.quarkus.scheduler.Scheduled
import java.time.OffsetDateTime
import java.time.OffsetDateTime.now
import java.time.ZoneOffset
import java.time.format.DateTimeFormatterBuilder
import java.time.temporal.ChronoField
import java.util.concurrent.TimeUnit
import javax.enterprise.context.ApplicationScoped
import javax.inject.Inject
import kotlin.math.roundToInt

@ApplicationScoped
class SplitService {
    @Inject
    lateinit var polygonService: PolygonService

    @Inject
    lateinit var polygonRestClient: PolygonRestClient

    @Inject
    lateinit var stockPreset: StockPreset

    @Inject
    lateinit var splitRepository: SplitRepository

    @Inject
    lateinit var transactionService: TransactionService

    @Inject
    lateinit var depotService: DepotService

    @Inject
    lateinit var finnhubService: FinnhubService

    private val dateTimeFormatter = DateTimeFormatterBuilder()
        .appendPattern("yyyy-MM-dd")
        .parseDefaulting(ChronoField.HOUR_OF_DAY, 0)
        .parseDefaulting(ChronoField.MINUTE_OF_HOUR, 0)
        .parseDefaulting(ChronoField.SECOND_OF_MINUTE, 0)
        .parseDefaulting(ChronoField.MILLI_OF_SECOND, 0)
        .toFormatter()
        .withZone(ZoneOffset.UTC)

    @Scheduled(every = "P1D", delay = 15, delayUnit = TimeUnit.MINUTES)
    fun checkNewSplitUpdates() {
        if (polygonService.getAPICallCount() > 5)
            return

        if (finnhubService.blockingRequester == APICallRequester.STOCK)
            return

        stockPreset.names.forEach {
            val stockSplit = polygonService.scheduleAPICall { polygonRestClient.getStockSplits(it) }

            stockSplit.results.forEach { entry -> splitRepository.insertSplit(toSplit(entry)) }
        }
    }

    @Scheduled(every = "1m")
    fun updateStockAmountOfUsers() {
        val stockSplits = splitRepository.getUpdatableSplits(now())

        stockSplits.forEach { split ->
            splitRepository.updateSplitExecuted(split)

            val depotEntries = depotService.getDepotEntries(split.symbol)
            depotEntries.forEach {
                transactionService.makeTransaction(
                    it.personId,
                    toTransactionWriteView(it.symbol, split.factor * it.amount)
                )
            }
        }
    }

    fun toTransactionWriteView(symbol: String, amount: Double): TransactionWriteView =
        TransactionWriteView(symbol, amount, TransactionType.SPLIT)

    fun toSplit(polygonStockSplitEntry: PolygonStockSplitEntry): Split {
        return with(polygonStockSplitEntry) {
            val factor = tofactor?.let { toFactor ->
                this.forfactor?.let { forFactor -> forFactor.toFloat() / toFactor.toFloat() }
            } ?: (1.0 / ratio)

            Split(
                ticker,
                factor.toDouble().roundToInt().toDouble(),
                OffsetDateTime.parse(exDate, dateTimeFormatter),
                false
            )
        }
    }
}
