package de.unigoettingen.tradinghero.split

import de.unigoettingen.trading_hero_core.jooq.tables.Split.SPLIT
import de.unigoettingen.trading_hero_core.jooq.tables.pojos.Split
import de.unigoettingen.tradinghero.database.Jooq
import de.unigoettingen.tradinghero.utils.DBHelper
import java.time.OffsetDateTime
import javax.enterprise.context.ApplicationScoped
import javax.inject.Inject

@ApplicationScoped
class SplitRepository {
    @Inject
    lateinit var jooq: Jooq

    fun insertSplit(split: Split) {
        jooq.dsl { db ->
            db.insertInto(SPLIT)
                .set(DBHelper.listProperties(split))
                .onConflictDoNothing()
                .execute()
        }
    }

    fun getSplit(symbol: String, factor: Double, date: OffsetDateTime): Split? = jooq.dsl { db ->
        db.selectFrom(SPLIT)
            .where(
                SPLIT.SYMBOL.eq(symbol)
                    .and(SPLIT.FACTOR.eq(factor))
                    .and(SPLIT.DATE.eq(date))
            )
            .fetchOneInto(Split::class.java)
    }

    fun getUpdatableSplits(time: OffsetDateTime): List<Split> = jooq.dsl { db ->
        db.selectFrom(SPLIT)
            .where(SPLIT.EXECUTED.eq(false).and(SPLIT.DATE.le(time)))
            .fetchInto(Split::class.java)
    }

    fun updateSplitExecuted(split: Split) = jooq.dsl { db ->
        db.update(SPLIT)
            .set(SPLIT.EXECUTED, true)
            .where(
                SPLIT.SYMBOL.eq(split.symbol)
                    .and(SPLIT.DATE.eq(split.date))
                    .and(SPLIT.FACTOR.eq(split.factor))
            )
            .execute()
    }
}
