package de.unigoettingen.tradinghero.utils

import org.jooq.impl.DSL.field
import org.jooq.impl.DSL.name
import kotlin.reflect.KClass
import kotlin.reflect.full.memberProperties
import kotlin.reflect.jvm.isAccessible

object DBHelper {
    // Lists all properties of a jooq generated object in (field to value) form, use it with the set() method
    fun <R : Any> listProperties(entity: R) =
        (entity::class as KClass<R>).memberProperties.associate {
            it.isAccessible = true
            field(name(it.name.camelToSnakeCase())) to it.get(entity)
        }.filterValues { value -> value != null }

    // https://stackoverflow.com/questions/60010298/how-can-i-convert-a-camel-case-string-to-snake-case-and-back-in-idiomatic-kotlin
    private val camelRegex = "(?<=[a-zA-Z])[A-Z]".toRegex()
    private val snakeRegex = "_[a-zA-Z]".toRegex()

    fun String.camelToSnakeCase(): String {
        return camelRegex.replace(this) {
            "_${it.value}"
        }.toLowerCase()
    }

    fun String.snakeToLowerCamelCase(): String {
        return snakeRegex.replace(this) {
            it.value.replace("_", "")
                .toUpperCase()
        }
    }
}
