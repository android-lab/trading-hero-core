package de.unigoettingen.tradinghero.utils

import org.eclipse.microprofile.config.inject.ConfigProperty
import javax.enterprise.context.ApplicationScoped
import javax.enterprise.inject.Instance

@ApplicationScoped
class Configuration {
    @ConfigProperty(name = "trading.hero.security", defaultValue = "enabled")
    lateinit var security: Instance<String>

    @ConfigProperty(name = "trading.hero.startbalance", defaultValue = "1000")
    lateinit var startBalance: Instance<Double>

    @ConfigProperty(name = "trading.hero.basecurrency", defaultValue = "USD")
    lateinit var baseCurrency: Instance<String>

    @ConfigProperty(name = "google.auth.clientid")
    lateinit var clientId: Instance<String>

    @ConfigProperty(name = "polygon.token")
    lateinit var polygonToken: Instance<String>

    @ConfigProperty(name = "finnhub.token")
    lateinit var finnhubToken: Instance<String>
}
