package de.unigoettingen.tradinghero.utils

import de.unigoettingen.trading_hero_core.jooq.tables.pojos.Course
import de.unigoettingen.trading_hero_core.jooq.tables.pojos.Person
import de.unigoettingen.trading_hero_core.jooq.tables.pojos.Split
import de.unigoettingen.trading_hero_core.jooq.tables.pojos.Stock
import de.unigoettingen.tradinghero.transactions.TransactionType
import de.unigoettingen.tradinghero.transactions.TransactionWriteView
import java.sql.Date
import java.time.OffsetDateTime.now

object TestDataProvider {
    val stock: Stock = Stock(
        "SYMBL",
        "EUR",
        "TestStock",
        "Eine Aktie fuer Tests",
        "1",
        "1",
        "SYMBL",
        "type",
        "XETRA",
        "Testing",
        Date(System.currentTimeMillis()),
        "url",
        1.0,
        1.0,
        "url"
    )

    val person: Person = Person(
        "1",
        "Tester",
        "test@host.de",
        "testbild.com",
        "Tester",
        "Chuck",
        now(),
    )

    val transactionWriteView = TransactionWriteView(stock.symbol, 20.0, TransactionType.BUY)

    val split = Split(stock.symbol, 10.0, now(), false)

    val course = Course(
        stock.symbol,
        0L,
        0.0
    )
}
