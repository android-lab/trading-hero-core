package de.unigoettingen.tradinghero.exceptions

import javax.ws.rs.InternalServerErrorException

class BalanceNotInitializedException : InternalServerErrorException()
