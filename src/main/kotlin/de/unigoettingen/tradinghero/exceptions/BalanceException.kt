package de.unigoettingen.tradinghero.exceptions

import javax.ws.rs.ClientErrorException

class BalanceException(message: String? = null) : ClientErrorException(message, 422)
