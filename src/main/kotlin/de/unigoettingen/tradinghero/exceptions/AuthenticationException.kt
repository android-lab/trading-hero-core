package de.unigoettingen.tradinghero.exceptions

import io.quarkus.security.UnauthorizedException

class AuthenticationException(s: String? = null) : UnauthorizedException(s)
