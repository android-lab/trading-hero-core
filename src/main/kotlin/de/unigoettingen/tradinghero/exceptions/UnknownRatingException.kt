package de.unigoettingen.tradinghero.exceptions

import javax.ws.rs.InternalServerErrorException

class UnknownRatingException(message: String? = null) : InternalServerErrorException(message)
