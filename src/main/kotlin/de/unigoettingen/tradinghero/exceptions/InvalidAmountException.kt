package de.unigoettingen.tradinghero.exceptions

import javax.ws.rs.ClientErrorException

class InvalidAmountException(message: String? = null) : ClientErrorException(message, 422)
