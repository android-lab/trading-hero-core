package de.unigoettingen.tradinghero.exceptions

import javax.ws.rs.InternalServerErrorException

class UnknownPriceException(message: String? = null) : InternalServerErrorException(message)
