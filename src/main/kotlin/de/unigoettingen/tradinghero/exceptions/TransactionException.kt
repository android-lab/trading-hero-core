package de.unigoettingen.tradinghero.exceptions

import javax.ws.rs.ClientErrorException

class TransactionException(message: String? = null) : ClientErrorException(message, 422)
