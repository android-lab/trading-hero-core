package de.unigoettingen.tradinghero.depot

import de.unigoettingen.trading_hero_core.jooq.tables.pojos.DepotEntry
import de.unigoettingen.tradinghero.balance.BalanceService
import de.unigoettingen.tradinghero.course.CourseRepository
import de.unigoettingen.tradinghero.course.CourseService
import de.unigoettingen.tradinghero.currency.CurrencyService
import de.unigoettingen.tradinghero.exceptions.TransactionException
import de.unigoettingen.tradinghero.friends.FriendshipService
import de.unigoettingen.tradinghero.transactions.TransactionType
import de.unigoettingen.tradinghero.transactions.TransactionWriteView
import de.unigoettingen.tradinghero.utils.Configuration
import javax.enterprise.context.ApplicationScoped
import javax.inject.Inject
import javax.ws.rs.ForbiddenException

@ApplicationScoped
class DepotService {

    @Inject
    lateinit var depotRepository: DepotRepository

    @Inject
    lateinit var courseRepository: CourseRepository

    @Inject
    lateinit var balanceService: BalanceService

    @Inject
    lateinit var currencyService: CurrencyService

    @Inject
    lateinit var configuration: Configuration

    @Inject
    lateinit var friendshipService: FriendshipService

    @Inject
    lateinit var courseService: CourseService

    fun getDepotBalance(requesterId: String, balanceUserId: String, currency: String?): Double? {
        if (!friendshipService.areFriends(requesterId, balanceUserId)) {
            throw ForbiddenException("No friendship available")
        }

        return getDepot(balanceUserId)
            .sumByDouble {
                courseService
                    .getLastStockPrice(it.symbol, currency)?.price?.times(it.amount) ?: return 0.0
            } + (balanceService.getBalance(balanceUserId, currency)?.amount ?: return null)
    }

    fun updateDepot(personId: String, transactionWriteView: TransactionWriteView): DepotEntry? =
        when (transactionWriteView.type) {
            TransactionType.BUY -> updateDepotWithBuy(
                personId,
                transactionWriteView
            )
            TransactionType.SELL -> updateDepotWithSell(
                personId,
                transactionWriteView
            )
            TransactionType.SPLIT -> updateDepotWithSplit(
                personId,
                transactionWriteView
            )
        }

    private fun updateDepotWithBuy(
        personId: String,
        transactionWriteView: TransactionWriteView
    ): DepotEntry? {
        with(transactionWriteView) {
            balanceService.addToBalance(
                personId,
                (courseRepository.getLastStockPrice(symbol)?.price?.times(-amount))
                    ?: throw TransactionException("No last course price!")

            )
            val currentAmount = depotRepository.getDepotEntry(personId, symbol)?.amount
            return if (currentAmount == null) {
                depotRepository.addEntry(DepotEntry(symbol, personId, amount))
            } else {
                depotRepository.updateEntry(
                    DepotEntry(
                        symbol,
                        personId,
                        currentAmount + amount
                    )
                )
            }
        }
    }

    private fun updateDepotWithSplit(
        personId: String,
        transactionWriteView: TransactionWriteView
    ): DepotEntry? {
        with(transactionWriteView) {
            return depotRepository.updateEntry(
                DepotEntry(
                    symbol,
                    personId,
                    amount
                )
            )
        }
    }

    private fun updateDepotWithSell(
        personId: String,
        transactionWriteView: TransactionWriteView
    ): DepotEntry? {
        with(transactionWriteView) {
            val currentAmount = depotRepository.getDepotEntry(personId, symbol)?.amount
                ?: throw TransactionException("Can not sell if no amount is available")

            if (currentAmount - amount < 0) {
                throw TransactionException("Can not sell more than available amount")
            }
            balanceService.addToBalance(
                personId,
                courseRepository.getLastStockPrice(symbol)?.price?.times(amount)
                    ?: throw TransactionException("No last course price!")
            )

            return depotRepository.updateEntry(DepotEntry(symbol, personId, currentAmount - amount))
        }
    }

    fun getDepot(personId: String) =
        depotRepository.getDepot(personId)

    fun getDepotEntry(personId: String, symbol: String) =
        depotRepository.getDepotEntry(personId, symbol)

    fun getDepotEntries(symbol: String) = depotRepository.getDepotEntries(symbol)
}
