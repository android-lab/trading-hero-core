package de.unigoettingen.tradinghero.depot

data class DepotView(val personId: String, val entries: List<DepotEntryView>)
