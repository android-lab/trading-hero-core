package de.unigoettingen.tradinghero.depot

import de.unigoettingen.trading_hero_core.jooq.tables.pojos.DepotEntry
import de.unigoettingen.tradinghero.auth.getUserId
import de.unigoettingen.tradinghero.balance.BalanceView
import javax.inject.Inject
import javax.ws.rs.*
import javax.ws.rs.core.Context
import javax.ws.rs.core.MediaType
import javax.ws.rs.core.SecurityContext

@Path("/depot")
@Produces(MediaType.APPLICATION_JSON)
class DepotResource {

    @Inject
    lateinit var depotService: DepotService

    @Context
    lateinit var ctx: SecurityContext

    @GET
    fun getDepot(): DepotView {
        val personId = getUserId(ctx)
        return toView(personId, depotService.getDepot(personId))
    }

    @GET
    @Path("/balance/{personId}")
    fun getBalance(@PathParam("personId") personId: String, @QueryParam("currency") currency: String?): BalanceView? {
        return depotService.getDepotBalance(getUserId(ctx), personId, currency)?.let {
            BalanceView(personId, it)
        }
    }

    @GET
    @Path("/{symbol}")
    fun getDepotEntry(@PathParam("symbol") symbol: String): DepotEntryView {
        val personId = getUserId(ctx)
        return toDepotEntryView(
            depotService.getDepotEntry(personId, symbol) ?: DepotEntry(
                symbol,
                personId,
                0.0
            )
        )
    }

    companion object {
        fun toView(personId: String, entries: List<DepotEntry>): DepotView =
            DepotView(personId, entries.map { toDepotEntryView(it) })

        fun toDepotEntryView(depotEntry: DepotEntry): DepotEntryView {
            return with(depotEntry) {
                DepotEntryView(
                    this.amount,
                    this.symbol
                )
            }
        }
    }
}
