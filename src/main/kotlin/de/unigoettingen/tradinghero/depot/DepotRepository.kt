package de.unigoettingen.tradinghero.depot

import de.unigoettingen.trading_hero_core.jooq.tables.DepotEntry.DEPOT_ENTRY
import de.unigoettingen.trading_hero_core.jooq.tables.pojos.DepotEntry
import de.unigoettingen.tradinghero.database.Jooq
import de.unigoettingen.tradinghero.utils.DBHelper
import javax.enterprise.context.ApplicationScoped
import javax.inject.Inject

@ApplicationScoped
class DepotRepository {

    @Inject
    lateinit var jooq: Jooq

    fun getDepot(personId: String): List<DepotEntry> = jooq.dsl { db ->
        db.selectFrom(DEPOT_ENTRY)
            .where(DEPOT_ENTRY.PERSON_ID.eq(personId))
            .fetchInto(DepotEntry::class.java)
    }

    fun getDepotEntry(personId: String, symbol: String): DepotEntry? = jooq.dsl { db ->
        db.selectFrom(DEPOT_ENTRY)
            .where(DEPOT_ENTRY.PERSON_ID.eq(personId).and(DEPOT_ENTRY.stock().SYMBOL.eq(symbol)))
            .fetchOneInto(DepotEntry::class.java)
    }

    fun getDepotEntries(symbol: String): List<DepotEntry> = jooq.dsl { db ->
        db.selectFrom(DEPOT_ENTRY)
            .where(DEPOT_ENTRY.SYMBOL.eq(symbol))
            .fetchInto(DepotEntry::class.java)
    }

    fun addEntry(entry: DepotEntry): DepotEntry? = jooq.dsl { db ->
        db.insertInto(DEPOT_ENTRY)
            .set(DBHelper.listProperties(entry))
            .returning()
            .fetchOneInto(DepotEntry::class.java)
    }

    fun updateEntry(entry: DepotEntry): DepotEntry? = jooq.dsl { db ->
        db.update(DEPOT_ENTRY)
            .set(DEPOT_ENTRY.AMOUNT, entry.amount)
            .where(DEPOT_ENTRY.SYMBOL.eq(entry.symbol).and(DEPOT_ENTRY.PERSON_ID.eq(entry.personId)))
            .returning()
            .fetchOneInto(DepotEntry::class.java)
    }
}
