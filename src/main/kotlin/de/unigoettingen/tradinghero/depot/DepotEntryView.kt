package de.unigoettingen.tradinghero.depot

data class DepotEntryView(val amount: Double, val symbol: String)
