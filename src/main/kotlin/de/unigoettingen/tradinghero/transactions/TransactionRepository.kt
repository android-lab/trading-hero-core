package de.unigoettingen.tradinghero.transactions

import de.unigoettingen.trading_hero_core.jooq.tables.Transaction.TRANSACTION
import de.unigoettingen.trading_hero_core.jooq.tables.pojos.Transaction
import de.unigoettingen.tradinghero.database.Jooq
import java.time.OffsetDateTime
import java.util.*
import javax.enterprise.context.ApplicationScoped
import javax.inject.Inject

@ApplicationScoped
class TransactionRepository {
    @Inject
    lateinit var jooq: Jooq

    fun addTransaction(personId: String, price: Double, date: OffsetDateTime, transactionWriteView: TransactionWriteView): Transaction? =
        with(transactionWriteView) {
            jooq.dsl { db ->
                db.insertInto(TRANSACTION)
                    .values(
                        UUID.randomUUID(),
                        symbol,
                        personId,
                        amount,
                        price,
                        type.db,
                        date
                    ).returning().fetchOneInto(Transaction::class.java)
            }
        }

    fun getTransaction(id: UUID) =
        jooq.dsl { db -> db.selectFrom(TRANSACTION).where(TRANSACTION.ID.eq(id)).fetchOneInto(Transaction::class.java) }

    fun getTransactions(personId: String, symbol: String): List<Transaction> = jooq.dsl { db ->
        db.selectFrom(TRANSACTION).where(TRANSACTION.PERSON_ID.eq(personId).and(TRANSACTION.SYMBOL.eq(symbol)))
            .fetchInto(Transaction::class.java)
    }
}
