package de.unigoettingen.tradinghero.transactions

import java.time.OffsetDateTime
import java.util.*

data class TransactionView(
    val id: UUID,
    val amount: Double,
    val price: Double,
    val type: TransactionType,
    val symbol: String,
    val date: OffsetDateTime
)
