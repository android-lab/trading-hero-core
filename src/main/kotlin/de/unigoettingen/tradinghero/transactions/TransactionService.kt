package de.unigoettingen.tradinghero.transactions

import de.unigoettingen.trading_hero_core.jooq.tables.pojos.Transaction
import de.unigoettingen.tradinghero.course.CourseRepository
import de.unigoettingen.tradinghero.depot.DepotService
import de.unigoettingen.tradinghero.exceptions.UnknownPriceException
import java.time.OffsetDateTime
import java.util.UUID
import javax.enterprise.context.ApplicationScoped
import javax.inject.Inject

@ApplicationScoped
class TransactionService {

    @Inject
    lateinit var depotService: DepotService

    @Inject
    lateinit var courseRepository: CourseRepository

    @Inject
    lateinit var transactionRepository: TransactionRepository

    fun makeTransaction(personId: String, transactionWriteView: TransactionWriteView): Transaction? {
        with(transactionWriteView) {
            val price = courseRepository.getLastStockPrice(symbol)?.price
                ?: throw UnknownPriceException("Could not get price for selected stock")

            depotService.updateDepot(personId, transactionWriteView)

            return transactionRepository.addTransaction(
                personId,
                price,
                OffsetDateTime.now(),
                transactionWriteView
            )
        }
    }

    fun getTransaction(id: UUID) = transactionRepository.getTransaction(id)

    fun getTransactionHistory(personId: String, symbol: String): List<Transaction> =
        transactionRepository.getTransactions(personId, symbol)
}
