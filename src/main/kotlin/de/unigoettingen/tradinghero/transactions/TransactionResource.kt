package de.unigoettingen.tradinghero.transactions

import de.unigoettingen.trading_hero_core.jooq.tables.pojos.Transaction
import de.unigoettingen.tradinghero.auth.getUserId
import java.util.*
import javax.inject.Inject
import javax.ws.rs.*
import javax.ws.rs.core.Context
import javax.ws.rs.core.MediaType
import javax.ws.rs.core.SecurityContext

@Path("/transaction")
@Produces(MediaType.APPLICATION_JSON)
class TransactionResource {

    @Inject
    lateinit var transactionService: TransactionService

    @Context
    lateinit var ctx: SecurityContext

    @POST
    fun makeTransaction(transactionWriteView: TransactionWriteView): TransactionView? {
        return (transactionService.makeTransaction(getUserId(ctx), transactionWriteView)?.let { toView(it) })
    }

    @GET
    @Path("/{transactionId}")
    fun getTransaction(@PathParam("transactionId") transactionId: UUID): TransactionView? {
        val transaction = transactionService.getTransaction(transactionId)
        if (transaction != null && transaction.personId != getUserId(ctx)) {
            throw ForbiddenException("One can only read their own transactions")
        }
        return transaction?.let { toView(it) }
    }

    @GET
    @Path("/history/{symbol}")
    fun getTransactionHistory(@PathParam("symbol") symbol: String): TransactionsListView =
        TransactionsListView(
            getUserId(ctx),
            symbol,
            transactionService.getTransactionHistory(getUserId(ctx), symbol).map { toView(it) }
        )

    companion object {
        fun toView(transaction: Transaction): TransactionView =
            TransactionView(
                transaction.id,
                transaction.amount,
                transaction.price,
                TransactionType.fromDB(transaction.type),
                transaction.symbol,
                transaction.date
            )
    }
}
