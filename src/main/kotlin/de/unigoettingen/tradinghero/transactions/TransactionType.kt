package de.unigoettingen.tradinghero.transactions

import de.unigoettingen.trading_hero_core.jooq.enums.TransactionTypeEnum

enum class TransactionType(val db: String) {
    BUY("buy"),
    SELL("sell"),
    SPLIT("split");

    companion object {
        fun fromDB(enum: TransactionTypeEnum): TransactionType =
            when (enum) {
                TransactionTypeEnum.buy -> BUY
                TransactionTypeEnum.sell -> SELL
                TransactionTypeEnum.split -> SPLIT
            }
    }
}
