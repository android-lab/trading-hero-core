package de.unigoettingen.tradinghero.transactions

data class TransactionWriteView(val symbol: String, val amount: Double, val type: TransactionType)
