package de.unigoettingen.tradinghero.transactions

data class TransactionsListView(val personId: String, val symbol: String, val transactions: List<TransactionView>)
