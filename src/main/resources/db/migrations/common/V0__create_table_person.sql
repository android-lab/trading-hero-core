CREATE TABLE person
(
    id varchar(255) PRIMARY KEY,
    name VARCHAR(255),
    email VARCHAR(255),
    image VARCHAR(255),
    family_name VARCHAR(255),
    given_name VARCHAR(255),
    date_created DATE
)