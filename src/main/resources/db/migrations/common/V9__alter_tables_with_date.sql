ALTER TABLE friendship
    ALTER COLUMN date_accepted TYPE TIMESTAMP WITH TIME ZONE;
ALTER TABLE friendship
    ALTER COLUMN date_created TYPE TIMESTAMP WITH TIME ZONE;
ALTER TABLE transaction
    ALTER COLUMN date TYPE TIMESTAMP WITH TIME ZONE;
ALTER TABLE person
    ALTER COLUMN date_created TYPE TIMESTAMP WITH TIME ZONE;
ALTER TABLE split
    ALTER COLUMN date TYPE TIMESTAMP WITH TIME ZONE;
ALTER TABLE rating
    ALTER COLUMN date TYPE TIMESTAMP WITH TIME ZONE;