CREATE TABLE stock
(
    symbol               varchar(10) PRIMARY KEY,
    currency             varchar(10),
    name                 varchar(255),
    description          varchar(255),
    figi                 varchar(255),
    mic                  varchar(10),
    display_symbol       varchar(10),
    type                 varchar(127),
    exchange             varchar(127),
    industry             varchar(255),
    ipo                  DATE,
    logo                 varchar(255),
    marketCapitalization FLOAT,
    shareOutstanding     FLOAT,
    url                  varchar(255)
)