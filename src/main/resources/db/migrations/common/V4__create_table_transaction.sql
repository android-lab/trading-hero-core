CREATE TYPE transaction_type_enum AS ENUM ('buy', 'sell', 'split');

CREATE EXTENSION IF NOT EXISTS pgcrypto;


CREATE TABLE transaction
(
    id        uuid PRIMARY KEY,
    symbol    varchar(10) REFERENCES stock ON DELETE CASCADE,
    person_id varchar(255) REFERENCES person ON DELETE CASCADE,
    amount    FLOAT,
    price     FLOAT,
    type      transaction_type_enum,
    date      DATE
)