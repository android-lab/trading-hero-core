CREATE TABLE rating
(
    stock   varchar(10) REFERENCES stock ON DELETE CASCADE,
    date    DATE,
    reddit  FLOAT,
    twitter FLOAT,
    rating  FLOAT,
    PRIMARY KEY (stock, date)
)