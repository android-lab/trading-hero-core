CREATE TABLE balance
(
    person_id varchar(255) REFERENCES person ON DELETE CASCADE,
    amount    FLOAT,
    PRIMARY KEY (person_id)
)