CREATE TABLE depot_entry
(
    symbol varchar(10) REFERENCES stock ON DELETE CASCADE,
    person_id varchar(255) REFERENCES person ON DELETE CASCADE,
    amount  FLOAT,
    PRIMARY KEY (symbol, person_id)
)