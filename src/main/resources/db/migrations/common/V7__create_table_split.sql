CREATE TABLE split
(
    symbol   varchar(10) REFERENCES stock ON DELETE CASCADE,
    factor   FLOAT,
    date     DATE,
    executed BOOL,
    PRIMARY KEY (symbol, factor, date)
)