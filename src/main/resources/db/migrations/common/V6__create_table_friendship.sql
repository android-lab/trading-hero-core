CREATE TABLE friendship
(
    person_id varchar(255) REFERENCES person ON DELETE CASCADE,
    friend_id varchar(255) REFERENCES person ON DELETE CASCADE,
    date_accepted DATE,
    date_created DATE,
    PRIMARY KEY (person_id, friend_id)
)