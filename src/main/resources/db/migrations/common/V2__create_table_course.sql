CREATE TABLE course
(
    symbol    varchar(10) REFERENCES stock ON DELETE CASCADE,
    timestamp BIGINT,
    price     FLOAT,
    PRIMARY KEY (symbol, timestamp)
)