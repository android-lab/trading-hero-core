FROM openjdk:11-jdk
RUN mkdir -p /app && \
    chown -R daemon /app

USER daemon
MAINTAINER trading_hero 
WORKDIR /app

ENV QUARKUS_DATASOURCE_JDBC_URL=jdbc:postgresql://srtf.dev:5432/core
ENV TRADING_HERO_SECURITY=enabled
EXPOSE 8080

COPY ./build/quarkus-app/ /app/

CMD ["java", "-jar", "/app/quarkus-run.jar"]
